function A = area(x,_grid)
  A=0;
  for i=1:size(_grid)(1)
    A+=norm(cross([x(_grid(i,2),1)-x(_grid(i,1),1)
		   x(_grid(i,2),2)-x(_grid(i,1),2)
		   x(_grid(i,2),3)-x(_grid(i,1),3)],
		  [x(_grid(i,3),1)-x(_grid(i,1),1)
		   x(_grid(i,3),2)-x(_grid(i,1),2)
		   x(_grid(i,3),3)-x(_grid(i,1),3)]),2);
  endfor
  A/=2;
endfunction
