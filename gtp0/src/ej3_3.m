function ej3_3
  Lx=1;
  Ly=1;
  x=0:Lx/100:Lx-Lx/100;
  y=0:Ly/100:Ly-Ly/100;

  E=200;
  nu=0.28;
  Fx=zeros(100,100);
  Fy=zeros(100,100);
  C0=-0.1*pi^2*E/(1-nu^2);
  CpiLx=pi/Lx;
  CpiLy=pi/Ly;
  Cx1=1/(Lx^2)+(1-nu)/(4*Ly^2);
  Cy1=1/(Ly^2)+(1-nu)/(4*Lx^2);
  C2=1/(2*Lx*Ly);
  for i=1:100, j=1:100;
    Fx(i,j) = C0*(sin(CpiLx*x(i))*cos(CpiLy*y(j)/2)*Cx1 \
		  +cos(CpiLx*x(i)/2)*C2*sin(CpiLy*y(j)));
    Fy(i,j) = C0*(sin(CpiLx*x(i)/2)*cos(CpiLy*y(j))*Cy1 \
		  +sin(CpiLy*y(j)/2)*C2*cos(CpiLx*x(i)));
  endfor;
  figure;
  mesh(x,y,Fx);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$F_x(x,y)$");
  title("$F_x$");
  print -dtex "img/ej3_3a.tex";
  figure;
  mesh(x,y,Fy);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$F_y(x,y)$");
  title("$F_y$");
  print -dtex "img/ej3_3b.tex";
  # gráfico de campo vectorial Fx,Fy
  figure;
  h = quiver(x(1:5:end),y(1:5:end),Fx(1:5:end,1:5:end),Fy(1:5:end,1:5:end));
  ha = gca();
  set(ha,"xlim",[0,1]);
  set(ha,"ylim",[0,1]);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$F_y(x,y)$");
  title("$F_y$");
  print -dtex "img/ej3_3c.tex";
endfunction
