function ej3
  Lx=1;
  Ly=1;
  x=0:Lx/100:Lx-Lx/100;
  y=0:Ly/100:Ly-Ly/100;
  ex=zeros(100,100);
  ey=zeros(100,100);
  exy=zeros(100,100);
  for i=1:100, j=1:100;
    ex(i,j) =(cos(pi*y(j)/(2*Ly))*(-0.1))'*(cos(pi*x(i)/Lx)*(pi/Lx)); #'
    ey(i,j) =(sin(pi*x(i)/(2*Lx))*0.1).*(sin(pi*y(j)/Ly)*(pi/Ly));
    exy(i,j)=(sin(pi*x(i)/Lx)*0.1).*(sin(pi*y(j)/(2*Ly))*(pi/(2*Ly))) \
            -(cos(pi*y(j)/Ly)*0.1).*(cos(pi*x(i)/(2*Lx))*(pi/(2*Lx)));
  endfor;
  figure;
  mesh(x,y,ex);
# xlabel("x*L");
# ylabel("q(x)");
  title("\\epsilon_x");
  figure;
  mesh(x,y,ey);
# xlabel("x*L");
# ylabel("q(x)");
  title("\\epsilon_y");
  figure;
  mesh(x,y,exy);
# xlabel("x*L");
# ylabel("q(x)");
  title("\\epsilon_{xy}");

  #
  # 2do item: stresses
  #
  E=200;
  nu=0.28;
  sx=zeros(100,100);
  sy=zeros(100,100);
  sxy=zeros(100,100);
  ca1=E/(1-nu^2); #const. auxiliar 1
  ca2=ca1*(1-nu); #const aux. 2 (para sxy)
  for i=1:100, j=1:100;
    sx(i,j) = ca1*(ex(i,j)+nu*ey(i,j));
    sy(i,j) = ca1*(nu*ex(i,j)+ey(i,j));
    sxy(i,j)= ca2*exy(i,j);
  endfor;
  figure;
  mesh(x,y,sx);
# xlabel("x*L");
# ylabel("q(x)");
  title("\\sigma_x");
  figure;
  mesh(x,y,sy);
# xlabel("x*L");
# ylabel("q(x)");
  title("\\sigma_y");
  figure;
  mesh(x,y,sxy);
# xlabel("x*L");
# ylabel("q(x)");
  title("\\sigma_{xy}");

  #
  # 3er item: fuerzas
  #
  Fx=zeros(100,100);
  Fy=zeros(100,100);
  C0=-0.1*pi^2*E/(1-nu^2);
  CpiLx=pi/Lx;
  CpiLy=pi/Ly;
  Cx1=1/(Lx^2)+(1-nu)/(4*Ly^2);
  Cy1=1/(Ly^2)+(1-nu)/(4*Lx^2);
  C2=1/(2*Lx*Ly);
  for i=1:100, j=1:100;
    Fx(i,j) = C0*(sin(CpiLx*x(i))*cos(CpiLy*y(j)/2)*Cx1 \
		  +cos(CpiLx*x(i)/2)*C2*sin(CpiLy*y(j)));
    Fy(i,j) = C0*(sin(CpiLx*x(i)/2)*cos(CpiLy*y(j))*Cy1 \
		  +sin(CpiLy*y(j)/2)*C2*cos(CpiLx*x(i)));
  endfor;
  figure;
  mesh(x,y,Fx);
# xlabel("x*L");
# ylabel("q(x)");
  title("\\F_x");
  figure;
  mesh(x,y,Fy);
# xlabel("x*L");
# ylabel("q(x)");
  title("\\F_y");
endfunction
