function ej1
  figure;
  hold on;
  set(0, "defaultlinelinewidth", 2);

  L=1;
  A=1/(exp(L)-exp(-L));
  x=0:L/1000:L-L/1000;

  T=A*exp(x)-A*exp(-x)+1;
  plot(x/L,T,"-r;$T(x)$;")
  
  q=-A*exp(x)-A*exp(-x);
  plot(x/L,q,"-b;$q(x)$;")

  xlabel("$x$");
  ylabel("$T(x)$, $q(x)$");
  #title("Soluci\\'on anal\\'itica T(x) y flujo q(x)");
  print -dtex "img/ej1.tex";
  print -depsc2 "img/ej1eps.eps";
  hold off;
endfunction