function [s,v] = tetra(x)
  s=zeros(4,3);
  v=0;
  s(1,:)=cross(x(2,:)-x(1,:),x(3,:)-x(2,:))/2;
  s(2,:)=cross(x(1,:)-x(2,:),x(4,:)-x(1,:))/2;
  s(3,:)=cross(x(4,:)-x(3,:),x(1,:)-x(4,:))/2;
  s(4,:)=cross(x(3,:)-x(4,:),x(2,:)-x(3,:))/2;
  v=norm((x(4,:)-x(1,:))*cross(x(4,:)-x(2,:),x(3,:)-x(4,:))',2)/6;
endfunction 