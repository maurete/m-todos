function M = pad( S, no=0, ea=0, we=0, so=0 )

  assert(no>=0,"error: arg menor a cero");
  assert(ea>=0,"error: arg menor a cero");
  assert(we>=0,"error: arg menor a cero");
  assert(so>=0,"error: arg menor a cero");

  wi = size(S)(2);
  he = size(S)(1);
  M = S;

  if ea > 0
    M = [ M zeros([he ea]) ];
  endif
  if we > 0
    M = [ zeros([he we]) M ];
  endif
  if no > 0
    M = [ zeros([no wi+we+ea]) ; M ];
  endif
  if we > 0
    M = [ zeros([so wi+we+ea]) ; M ];
  endif

endfunction
