# función: elastfem_2d_sta
# calcula la solución numérica al problema de ELASTICIDAD PLANA
# por el método de los elementos finitos LINEALES con elementos CUADRANGULARES

# la matriz node tiene la forma:
#          x     y
# nodo1:  ###   ###
# nodo2:  ###   ###
#         ...   ...
# nodoM:  ###   ###
#
# la matriz elem tiene la forma:
#        nodo1 nodo2 nodo3 nodo4
# elem1:  ###   ###   ###   ###
# elem2:  ###   ###   ###   ###
#         ...   ...   ...   ...
# elemN:  ###   ###   ###   ###
#
# las conds de borde Dirichlet (desplazamientos) tienen la forma:
#  nnodo   ux    uy
#   ###   ###   ###
#   ###   ###   ###
#   ...   ...   ...
#   ###   ###   ###
#
# las conds de borde Neumann (tracciones) tienen la forma:
#  nodo1 nodo2   tx    ty
#   ###   ###   ###   ###
#   ###   ###   ###   ###
#   ...   ...   ...   ...
#   ###   ###   ###   ###
#
# las fuerzas distribuidas (de cuerpo) vienen dadas por:
#          Fx    Fy
# elem1:  ###   ###
# elem2:  ###   ###
#         ...   ###
# elemN:  ###   ###
#
# las fuerzas puntuales vienen dadas por:
#    x     y     Fx    Fy
#   ###   ###   ###   ###
#   ###   ###   ###   ###
#   ...   ...   ...   ...
#   ###   ###   ###   ###

function [ a R ee zz node ] = elastfem_2d_sta_sq (
		    node,      # vector de nodos [ Mx2 ]
		    elem,      # vector de elems [ Nx4 ]
		    E,         # módulo de Young
		    nu,        # coeficiente de Poisson
		    t=1,       # espesor de la placa
		    Fp=[],     # fuerzas PUNTUALES: fila: x y Fx Fy
		    Fd=[],     # fuerzas DISTRIBUIDAS [ Nx2 ]
		    bcD=[],    # condiciones Dirichlet (borde empotrado)
		    bcN=[],    # condiciones Neumann (traccion en los bordes)
		    debug=0
		  )

  Nelems = size(elem)(1);
  Nnodes = size(node)(1);

  Kg = zeros(2*Nnodes);
  fg = zeros(2*Nnodes,1);
  Bg = zeros(3,2*Nnodes); # para calcular el strain resultante al final

  # función auxiliar, devuelve d1 si le pedis 1
  function re = pick(num,d1,d2,d3,d4)
    switch num
      case 1
	re=d1;
	return
      case 2
	re=d2;
	return
      case 3
	re=d3;
	return
      case 4
	re=d4;
	return
    endswitch
  endfunction
  
  for el=1:Nelems

    # numeracion local de los nodos
    x1 = node(elem(el,1),1); y1 = node(elem(el,1),2);
    x2 = node(elem(el,2),1); y2 = node(elem(el,2),2);
    x3 = node(elem(el,3),1); y3 = node(elem(el,3),2);
    x4 = node(elem(el,4),1); y4 = node(elem(el,4),2);

    a = (x2-x1)/2;
    b = (y3-y2)/2;
    ox = x1+a;
    oy = y1+b;

    # área del elemento
    Ae = 4*a*b;

    N1 = @(r,s) (1-r/a)*(1-s/b)/4;
    N2 = @(r,s) (1+r/a)*(1-s/b)/4;
    N3 = @(r,s) (1+r/a)*(1+s/b)/4;
    N4 = @(r,s) (1-r/a)*(1+s/b)/4;

    a1 = @(r,s) (1+r/a)/(4*b);
    a2 = @(r,s) (1-r/a)/(4*b);
    b1 = @(r,s) (1+s/b)/(4*a);
    b2 = @(r,s) (1-s/b)/(4*a);

    B1 = @(r,s) [ -b2(r,s) 0; 0 -a2(r,s); -a2(r,s) -b2(r,s) ];
    B2 = @(r,s) [  b2(r,s) 0; 0 -a1(r,s); -a1(r,s)  b2(r,s) ];
    B3 = @(r,s) [  b1(r,s) 0; 0  a1(r,s);  a1(r,s)  b1(r,s) ];
    B4 = @(r,s) [ -b1(r,s) 0; 0  a2(r,s);  a2(r,s) -b1(r,s) ];

    Be = @(i) pick(i,B1,B2,B3,B4)(pick(i,x1,x2,x3,x4)-ox,pick(i,y1,y2,y3,y4)-oy);

    D = (E/(1-nu^2))*[ 1 nu 0; nu 1 0; 0 0 (1-nu)/2 ];

    # matriz de rigidez elemental

    # auxiliares
    a_ = [ t*b*D(1,1)/(6*a) ;
	   t*a*D(2,2)/(6*b) ;
	   t * D(1,2)/4     ;
	   t*a*D(3,3)/(6*b) ;
	   t*b*D(3,3)/(6*a) ;
	   t * D(3,3)/4     ];

    ak = @(i,j) a_(i)+a_(j);
    bk = @(i,j) a_(i)-a_(j);
    ck = @(i,j) a_(i)-2*a_(j);

    # esto podría armarlo en términos de los Be(i), si es que están bien calculados
    kelem = [ 2*ak(1,1)  ak(3,6)  ck(4,1)  bk(3,6) -ak(1,4) -ak(3,6)  ck(1,4)  bk(6,3) ;
	         0     2*ak(3,5)  bk(6,3)  ck(2,5) -ak(3,6) -ak(2,5)  bk(3,6)  ck(5,2) ;
		 0        0     2*ak(1,4) -ak(3,6)  ck(1,4)  bk(3,6) -ak(1,4)  ak(6,3) ;
		 0        0        0     2*ak(2,5)  bk(6,3)  ck(5,2)  ak(3,6) -ak(5,2) ;
		 0        0        0        0     2*ak(1,4)  ak(3,6)  ck(4,1)  bk(6,3) ;
		 0        0        0        0        0     2*ak(2,5)  bk(6,3)  ck(2,5) ;
		 0        0        0        0        0        0     2*ak(1,1) -bk(3,6) ;
		 0        0        0        0        0        0        0     2*ak(2,5) ];

    for j=1:7
      for i=j+1:8
	kelem(i,j) = kelem(j,i);
      endfor
    endfor

    if debug
       printf("k elemental elem %d\n", el)
       kelem
    endif

    felem = [0;0;0;0;0;0;0;0];
    
    # agrego al RHS las fuerzas de cuerpo en el elemento
    if Fd(el,1)
        felem += [1;0;1;0;1;0;1;0]*Fd(el,1)*Ae*t/4;
    endif
    if Fd(el,2)
        felem += [0;1;0;1;0;1;0;1]*Fd(el,2)*Ae*t/4;
    endif
    
    if debug
       printf("f elemental solo fuerzas de cuerpo elem %d\n", el)
       felem
    endif
        
    delete = [];

    # interpolación de fuerzas puntuales
    # para cada fuerza puntual
    for i=1:size(Fp)(1)
      # coordenadas locales de la fuerza puntual
      r = Fp(i,1) - ox;
      s = Fp(i,2) - oy;
      # coordenadas ""baricentricas"" calculadas con los Nl
      bar = [ N1(r,s); N2(r,s); N3(r,s); N4(r,s) ];
      # si esta dentro del elemento
      if 0 < bar && bar < 1 && sum(bar)-1 < 0.0001
	if debug
	   printf("Interpolando Fp=(%f,%f) en elemento %d\n\n",Fp(i,3),Fp(i,4),el)
	endif
	# sumo al RHS la fuente puntual interpolada
	Fpx = Fp(i,3)*bar;
	Fpy = Fp(i,4)*bar;
        felem += [ Fpx(1); Fpy(1); Fpx(2); Fpy(2); Fpx(3); Fpy(3); Fpx(4); Fpy(4) ];
	# anoto para borrarla al final del for
	delete = [ delete i];
      endif
    endfor

    if debug
      printf("f elemental fuerza cuerpo+ puntual interpolada elem %d\n", el)
      felem
    endif

    # anulo las fuerzas puntuales ya consideradas
    for i=delete
      Fp(i,3) = 0;
      Fp(i,4) = 0;
    endfor
    # reconstruyo la matriz de fuerzas puntuales
    # borrando las que ya consideré (no puede estar en 2 elems a la vez!)
    Fp2 = [];
    for i=1:size(Fp)(1);
      if Fp(i,3) != 0 || Fp(i,4) != 0
	Fp2 = [Fp2; Fp(i,:)];
      endif
    endfor
    if Fp
    Fp = Fp2;
    endif

    # TRACCIONES
    # encuentro si las condiciones de contorno neumann
    # matchean alguna arista y guardo los valores
    neu = [ 0 0 ; #lado 12 X
	    0 0 ; #lado 41 Y
	    0 0 ; #lado 23 Y
	    0 0 ];#lado 34 X
    
    if bcN
    for i = 1:4
      for j = 1:4
	if j != i
	  # busco en la primer columna de bcN el nodo i
	  n1 = find( bcN(:,1) == elem(el,i) );
	  # busco en la segunda columna de bcN el nodo j
	  n2 = find( bcN(:,2) == elem(el,j) );

	  if n1 == n2
	    # guardo en neu la traccion en el lado corresp
	    neu(min(i*j/2,4),1) = bcN(n1,3);
	    neu(min(i*j/2,4),2) = bcN(n1,4);
	  endif

	endif
      endfor
    endfor
    endif

    ## if debug
    ##   printf("conds neumann en elem%d\n", el)
    ##   neu
    ## endif

    # lado 1--2
    if neu(1,1)!=0 || neu(1,2)!=0
      # long de la arista
      d = sqrt((x2-x1)^2+(y2-y1)^2);
      # componentes x y de la traccion
      tx = neu(1,1); ty = neu(1,2);
      # sumo al rhs el aporte correspondiente
      felem += d*t/2 * [ tx; ty; tx; ty; 0; 0; 0; 0 ];
    endif
    # lado 4--1
    if neu(2,1)!=0 || neu(2,2)!=0
      # long de la arista
      d = sqrt((x1-x4)^2+(y1-y4)^2);
      # componentes x y de la traccion
      tx = neu(2,1); ty = neu(2,2);
      # sumo al rhs el aporte correspondiente
      felem += d*t/2 * [ tx; ty; 0; 0; 0; 0; tx; ty ];
    endif
    # lado 2--3
    if neu(3,1)!=0 || neu(3,2)!=0
      # long de la arista
      d = sqrt((x3-x2)^2+(y3-y2)^2);
      # componentes x y de la traccion
      tx = neu(3,1); ty = neu(3,2);
      # sumo al rhs el aporte correspondiente
      felem += d*t/2 * [ 0; 0; tx; ty; tx; ty; 0; 0 ];
    endif
    # lado 3--4
    if neu(4,1)!=0 || neu(4,2)!=0
      # long de la arista
      d = sqrt((x4-x3)^2+(y4-y3)^2);
      # componentes x y de la traccion
      tx = neu(4,1); ty = neu(4,2);
      # sumo al rhs el aporte correspondiente
      felem += d*t/2 * [ 0; 0; 0; 0; tx; ty; tx; ty ];
    endif

    if debug
      printf("f elemental + conds borde neumann elem %d\n", el)
      felem
    endif


    # ASSEMBLE
    for i=1:4
      for j=1:4
	Kg( elem(el,i)*2-1,elem(el,j)*2-1 ) += kelem(i*2-1,j*2-1);
	Kg( elem(el,i)*2  ,elem(el,j)*2-1 ) += kelem(i*2  ,j*2-1);
	Kg( elem(el,i)*2-1,elem(el,j)*2   ) += kelem(i*2-1,j*2  );
	Kg( elem(el,i)*2  ,elem(el,j)*2   ) += kelem(i*2  ,j*2  );
      endfor
	fg( elem(el,i)*2-1) += felem(i*2-1);
	fg( elem(el,i)*2  ) += felem(i*2  );

	Bg(:,elem(el,i)*2-1:elem(el,i)*2) += Be(i);
    endfor

  endfor

  if debug
    printf("K global\n", el)
    Kg
  endif


  # Condiciones de contorno Dirichlet
  # piso la fila i correspondiente en la K global con todos ceros
  # y un uno en i. En el rhs pongo a mano el valor especificado.
  #
  # ii: contado auxiliar para recorrer bcD
  ii = 1;
  for i=[ [bcD(:,1)]*2]'
    valx=bcD(ii,2);
    valy=bcD(ii,3);
    Kg(i-1,:) = E*eye(2*Nnodes)(i-1,:);
    fg(i-1)   = E*valx;
    Kg(i,:)   = E*eye(2*Nnodes)(i,:);
    fg(i)     = E*valy;
    ii+=1;
  endfor

  if debug
    printf("K global con conds de borde Dirichlet incorporadas\n", el)
    Kg
    printf("f global\n", el)
    fg
  endif

  # SOLUCIÓN vector a de los desplazamientos
  a = Kg \ fg;

  # REACCIONES
  R = Kg*a;

  # STRAINS
  # ee = epsilon
  ep = Bg*diag(a);
  ee = ep(:,1:2:end)+ep(:,2:2:end);

  # STRESSES
  # zz = sigma
  zz = D*ee;
  
endfunction
