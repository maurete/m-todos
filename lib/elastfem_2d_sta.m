# función: heatfem_2d_sta
# calcula la solución numérica a la ecuación del calor en 2D ESTÁTICA SÓLO DIFUSIÓN CON FUENTE g
# por el método de los elementos finitos LINEALES con elementos TRIANGULARES

# ecuación del calor: k d²T/dx² + g = 0

# la matriz node tiene la forma:
#          x     y
# nodo1:  ###   ###
# nodo2:  ###   ###
#         ...   ...
# nodoM:  ###   ###
#
# la matriz elem tiene la forma:
#        nodo1 nodo2 nodo3
# elem1:  ###   ###   ###
# elem2:  ###   ###   ###
#         ...   ...   ...
# elemN:  ###   ###   ###
#
# las conds de borde Dirichlet (deformaciones) tienen la forma:
#  nnodo   ux    uy
#   ###   ###   ###
#   ###   ###   ###
#   ...   ...   ...
#   ###   ###   ###
#
# las conds de borde Neumann (strains o stresses?) tienen la forma:
#  nodo1 nodo2   tx    ty
#   ###   ###   ###   ###
#   ###   ###   ###   ###
#   ...   ...   ...   ...
#   ###   ###   ###   ###
#
# las fuerzas distribuidas (de cuerpo?) vienen dadas por:
#          Fx    Fy
# elem1:  ###   ###
# elem2:  ###   ###
#         ...   ###
# elemN:  ###   ###
#
# las fuerzas puntuales vienen dadas por:
#    x     y     Fx    Fy
#   ###   ###   ###   ###
#   ###   ###   ###   ###
#   ...   ...   ...   ...
#   ###   ###   ###   ###

function [ a R ee zz node ] = elastfem_2d_sta (
		    node,      # vector de nodos [ Mx2 ]
		    elem,      # vector de elems [ Nx3 ]
		    E,         # módulo de Young
		    nu,        # coeficiente de Poisson
		    t=1,       # espesor de la placa
		    Fp=[],     # fuerzas PUNTUALES: fila: x y Fx Fy
		    Fd=[],     # fuerzas DISTRIBUIDAS [ Nx2 ]
		    bcD=[],    # condiciones Dirichlet (borde empotrado)
		    bcN=[],    # condiciones Neumann (traccion en los bordes)
		    debug=0
		  )

  Nelems = size(elem)(1);
  Nnodes = size(node)(1);

  Kg = zeros(2*Nnodes);
  fg = zeros(2*Nnodes,1);
  Bg = zeros(3,2*Nnodes); # para calcular el strain resultante al final

  function r = pick(num,d1,d2,d3)
    switch num
      case 1
	r=d1;
	return
      case 2
	r=d2;
	return
      case 3
	r=d3;
	return
    endswitch
  endfunction
  
  for el=1:Nelems

    # numeracion local de los nodos
    x1 = node(elem(el,1),1); y1 = node(elem(el,1),2);
    x2 = node(elem(el,2),1); y2 = node(elem(el,2),2);
    x3 = node(elem(el,3),1); y3 = node(elem(el,3),2);

    # área del elemento
    Ae = det( [ 1 x1 y1; 1 x2 y2; 1 x3 y3] )/2;

    # Ni = (1/(2*Ae))*(ai+bi+ci)
    a1 = x2*y3-x3*y2; b1 = y2-y3; c1 = x3-x2;
    a2 = x3*y1-x1*y3; b2 = y3-y1; c2 = x1-x3;
    a3 = x1*y2-x2*y1; b3 = y1-y2; c3 = x2-x1;

    N1 = @(x,y) (a1+b1*x+c1*y)/(2*Ae);
    N2 = @(x,y) (a2+b2*x+c2*y)/(2*Ae);
    N3 = @(x,y) (a3+b3*x+c3*y)/(2*Ae);

    B1 = (1/(2*Ae))*[ b1 0; 0 c1; c1 b1 ];
    B2 = (1/(2*Ae))*[ b2 0; 0 c2; c2 b2 ];
    B3 = (1/(2*Ae))*[ b3 0; 0 c3; c3 b3 ];

    D = (E/(1-nu^2))*[ 1 nu 0; nu 1 0; 0 0 (1-nu)/2 ];

    # matriz elemental

    kelem = Ae*t * [ B1'*D*B1 B1'*D*B2 B1'*D*B3 ;
		     B2'*D*B1 B2'*D*B2 B2'*D*B3 ;
		     B3'*D*B1 B3'*D*B2 B3'*D*B3 ];

    if debug
       printf("k elemental elem %d\n", el)
       kelem
    endif

    felem = [0;0;0;0;0;0];
    
    # agrego al RHS las fuerzas de cuerpo en el elemento
    if Fd(el,1)
        felem += [1;0;1;0;1;0]*Fd(el,1)*Ae*t/3;
    endif
    if Fd(el,2)
        felem += [0;1;0;1;0;1]*Fd(el,2)*Ae*t/3;
    endif
    
    if debug
       printf("f elemental solo fuerzas de cuerpo elem %d\n", el)
       felem
    endif
        
    delete = [];

    # interpolación de fuerzas puntuales
    # para cada fuerza puntual
    for i=1:size(Fp)(1)
      # coordenadas reales de la fuerza puntual
      xp = Fp(i,1); yp = Fp(i,2);
      # coordenadas baricentricas calculadas con los Nl
      bar = [ N1(xp,yp); N2(xp,yp); N3(xp,yp) ];
      # si esta dentro del elemento
      if 0 < bar && bar < 1 && sum(bar)-1 < 0.0001
	if debug
	   printf("Interpolando Fp=(%f,%f) en elemento %d\n\n",Fp(i,3),Fp(i,4),el)
	endif
	# sumo al RHS la fuente puntual interpolada
	Fpx = Fp(i,3)*bar;
	Fpy = Fp(i,4)*bar;
        felem += [ Fpx(1); Fpy(1); Fpx(2); Fpy(2); Fpx(3); Fpy(3) ];
	# anoto para borrarla al final del for
	delete = [ delete i];
      endif
    endfor

    if debug
      printf("f elemental fuerza cuerpo+ puntual interpolada elem %d\n", el)
      felem
    endif

    # anulo las fuerzas puntuales ya consideradas
    for i=delete
      Fp(i,3) = 0;
      Fp(i,4) = 0;
    endfor
    # reconstruyo la matriz de fuerzas puntuales
    # borrando las que ya consideré (no puede estar en 2 elems a la vez!)
    Fp2 = [];
    for i=1:size(Fp)(1);
      if Fp(i,3) != 0 || Fp(i,4) != 0
	Fp2 = [Fp2; Fp(i,:)];
      endif
    endfor
    if Fp
    Fp = Fp2;
    endif

    # TRACCIONES
    # encuentro si las condiciones de contorno neumann
    # matchean alguna arista y guardo los valores
    neu = [ 0 0 ; #lado 12
	    0 0 ; #lado 31
	    0 0 ];#lado 23
    
    if bcN
    for i = 1:3
      for j = 1:3
	if j != i
	  # busco en la primer columna de bcN el nodo i
	  n1 = find( bcN(:,1) == elem(el,i) );
	  # busco en la segunda columna de bcN el nodo j
	  n2 = find( bcN(:,2) == elem(el,j) );

	  if n1 == n2
	    # guardo en neu la traccion en el lado corresp
	    neu(i+j-2,1) = bcN(n1,3);
	    neu(i+j-2,2) = bcN(n1,4);
	  endif

	endif
      endfor
    endfor
    endif
    ## if debug
    ##   printf("conds neumann en elem%d\n", el)
    ##   neu
    ## endif

    if neu(1,1)!=0 || neu(1,2)!=0
      # long de la arista
      d = sqrt((x2-x1)^2+(y2-y1)^2);
      # componentes x y de la traccion
      tx = neu(1,1); ty = neu(1,2);
      # sumo al rhs el aporte correspondiente
      felem += d*t/2 * [ tx; ty; tx; ty; 0; 0 ];
    endif
    if neu(2,1)!=0 || neu(2,2)!=0
      # long de la arista
      d = sqrt((x1-x3)^2+(y1-y3)^2);
      # componentes x y de la traccion
      tx = neu(2,1); ty = neu(2,2);
      # sumo al rhs el aporte correspondiente
      felem += d*t/2 * [ tx; ty; 0; 0; tx; ty ];
    endif
    if neu(3,1)!=0 || neu(3,2)!=0
      # long de la arista
      d = sqrt((x3-x2)^2+(y3-y2)^2);
      # componentes x y de la traccion
      tx = neu(3,1); ty = neu(3,2);
      # sumo al rhs el aporte correspondiente
      felem += d*t/2 * [ 0; 0; tx; ty; tx; ty ];
    endif

    if debug
      printf("f elemental + conds borde neumann elem %d\n", el)
      felem
    endif


    # ASSEMBLE
    for i=1:3
      for j=1:3
	Kg( elem(el,i)*2-1,elem(el,j)*2-1 ) += kelem(i*2-1,j*2-1);
	Kg( elem(el,i)*2  ,elem(el,j)*2-1 ) += kelem(i*2  ,j*2-1);
	Kg( elem(el,i)*2-1,elem(el,j)*2   ) += kelem(i*2-1,j*2  );
	Kg( elem(el,i)*2  ,elem(el,j)*2   ) += kelem(i*2  ,j*2  );
      endfor
	fg( elem(el,i)*2-1) += felem(i*2-1);
	fg( elem(el,i)*2  ) += felem(i*2  );

	Bg(:,elem(el,i)*2-1:elem(el,i)*2) += pick(i,B1,B2,B3);
    endfor

  endfor

  if debug
    printf("K global\n", el)
    Kg
  endif


  # Condiciones de contorno Dirichlet
  # piso la fila i correspondiente en la K global con todos ceros
  # y un uno en i. En el rhs pongo a mano el valor especificado.
  #
  # ii: contado auxiliar para recorrer bcD
  ii = 1;
  for i=[ [bcD(:,1)]*2]'
    valx=bcD(ii,2);
    valy=bcD(ii,3);
    Kg(i-1,:) = E*eye(2*Nnodes)(i-1,:);
    fg(i-1)   = E*valx;
    Kg(i,:)   = E*eye(2*Nnodes)(i,:);
    fg(i)     = E*valy;
    ii+=1;
  endfor

  if debug
    printf("K global con conds de borde Dirichlet incorporadas\n", el)
    Kg
    printf("f global\n", el)
    fg
  endif

  # SOLUCIÓN vector a de los desplazamientos
  a = Kg \ fg;

  # REACCIONES
  R = Kg*a;

  # STRAINS
  # ee = epsilon
  ep = Bg*diag(a);
  ee = ep(:,1:2:end)+ep(:,2:2:end);

  # STRESSES
  # zz = sigma
  zz = D*ee;
  
endfunction
