# función: heatfem_2d_sta
# calcula la solución numérica a la ecuación del calor en 2D ESTÁTICA SÓLO DIFUSIÓN CON FUENTE g
# por el método de los elementos finitos LINEALES con elementos TRIANGULARES

# ecuación del calor: k d²T/dx² + g = 0

# la matriz node tiene la forma:
#          x     y
# nodo1:  ###   ###
# nodo2:  ###   ###
#         ...   ...
# nodoM:  ###   ###
#
# la matriz elem tiene la forma:
#        nodo1 nodo2 nodo3
# elem1:  ###   ###   ###
# elem2:  ###   ###   ###
#         ...   ...   ...
# elemN:  ###   ###   ###
#
# las conds de borde Dirichlet tienen la forma:
#  nnodo valor
#   ###   ###
#   ###   ###
#   ...   ...
#   ###   ###
#
# las conds de borde Neumann tienen la forma:
#  nodo1 nodo2 qnormal
#   ###   ###   ###
#   ###   ###   ###
#   ...   ...   ...
#   ###   ###   ###
#
# las conds de borde convectivas tienen la forma:
#  nodo1 nodo2   c    Tamb
#   ###   ###   ###   ###
#   ###   ###   ###   ###
#   ...   ...   ...   ...
#   ###   ###   ###   ###
#
# la fuente de calor distribuida viene dada por:
#          qe
# elem1:  ###
# elem2:  ###
#         ...
# elemN:  ###
#
# las fuentes de calor puntual vienen dadas por:
#    x     y     qp
#   ###   ###   ###
#   ###   ###   ###
#   ...   ...   ...
#   ###   ###   ###

function [ T Kg fg ] = heatfem_2d_sta (
		    node,      # vector de nodos [ Mx2 ]
		    elem,      # vector de elems [ Nx3 ]
		    k=0,       # difusividad [ 1x1 o 1xN ]
		    gP=[],     # fuentes PUNTUALES: fila: x y valor
		    ge=[],     # fuentes DISTRIBUIDAS [ Nx1 ]
		    bcD=[],    # condiciones Dirichlet
		    bcN=[],    # condiciones Neumann
		    bcM=[],    # condiciones convectivas
		    debug=0
		  )

  Nelems = size(elem)(1);
  Nnodes = size(node)(1);

  Kg = zeros(Nnodes);
  fg = zeros(Nnodes,1);

  for el=1:Nelems

    # numeracion local de los nodos
    x1 = node(elem(el,1),1); y1 = node(elem(el,1),2);
    x2 = node(elem(el,2),1); y2 = node(elem(el,2),2);
    x3 = node(elem(el,3),1); y3 = node(elem(el,3),2);

    # área del elemento
    Ae = det( [ 1 x1 y1; 1 x2 y2; 1 x3 y3] )/2;

    # Ni = (1/(2*Ae))*(ai+bi+ci)
    a1 = x2*y3-x3*y2; b1 = y2-y3; c1 = x3-x2;
    a2 = x3*y1-x1*y3; b2 = y3-y1; c2 = x1-x3;
    a3 = x1*y2-x2*y1; b3 = y1-y2; c3 = x2-x1;

    N1 = @(x,y) (a1+b1*x+c1*y)/(2*Ae);
    N2 = @(x,y) (a2+b2*x+c2*y)/(2*Ae);
    N3 = @(x,y) (a3+b3*x+c3*y)/(2*Ae);

    # matriz elemental, sólo difusión
    kelem = k/(4*Ae) * [ b1^2+c1^2 b1*b2+c1*c2 b1*b3+c1*c3 ;
		            0      b2^2+c2^2  b2*b3+c2*c3 ;
			    0             0    b3^2+c3^2  ];
    kelem(2,1) = kelem(1,2);
    kelem(3,1) = kelem(1,3);
    kelem(3,2) = kelem(2,3);

    if debug
       printf("k elemental solo difusion elem %d\n", el)
       kelem
    endif

    felem = [0; 0; 0];
    
    # agrego al RHS la fuente distribuida en el elemento
    if ge(el)
        felem += ge(el)*Ae/3;
    endif
    
    if debug
       printf("f elemental solo fuente distrib elem %d\n", el)
       felem
    endif
        
    delete = [];

    # interpolación de fuentes puntuales
    # para cada fuente puntual
    for i=1:size(gP)(1)
      # coordenadas reales de la fuente puntual
      xp = gP(i,1); yp = gP(i,2);
      # coordenadas baricentricas calculadas con los Nl
      bar = [ N1(xp,yp); N2(xp,yp); N3(xp,yp) ];
      # si esta dentro del elemento
      if 0 < bar && bar < 1 && sum(bar)-1 < 0.0001
	# sumo al RHS la fuente puntual interpolada
	felem += gP(i,3) * bar;
	# anoto para borrarla al final del for
	delete = [ delete i];
      endif
    endfor

    if debug
      printf("f elemental fuente dist+ puntual interpolada elem %d\n", el)
      felem
    endif

    # anulo las fuentes ya consideradas
    for i=delete
      gP(i,3) = 0;
    endfor
    # reconstruyo la matriz de fuentes puntuales
    # borrando las que ya consideré
    gPneu = [];
    for i=1:size(gP)(1);
      if gP(i,3) != 0
	gPneu = [gPneu gP(i,:)];
      endif
    endfor
    gP = gPneu;


    # encuentro si las condiciones de contorno neumann, mixtas
    # matchean alguna arista y guardo los valores
    neu = [ 0 ; #lado 12
	    0 ; #lado 31
	    0 ];#lado 23

    mix = [ 0 0 ; #lado 12
	    0 0 ; #lado 31
	    0 0 ];#lado 23

    for i = 1:3
      for j = 1:3
	if j != i
	  n1 = find( bcN(:,1) == elem(el,i) );
	  n2 = find( bcN(:,2) == elem(el,j) );
	  m1 = find( bcM(:,1) == elem(el,i) );
	  m2 = find( bcM(:,2) == elem(el,j) );

	  if n1 == n2
	    neu(i+j-2) = bcN(n1,3);
	  endif

	  if m1 == m2
	    mix(i+j-2,1) = bcM(m1,3);
	    mix(i+j-2,2) = bcM(m1,4);
	  endif

	endif
      endfor
    endfor

    if neu(1)
      dist = sqrt((x2-x1)^2+(y2-y1)^2);
      felem -= neu(1)*dist*[1/2;1/2;0];
    endif
    if neu(2)
      dist = sqrt((x1-x3)^2+(y1-y3)^2);
      felem -= neu(2)*dist*[1/2;0;1/2];
    endif
    if neu(3)
      dist = sqrt((x3-x2)^2+(y3-y2)^2);
      felem -= neu(3)*dist*[0;1/2;1/2];
    endif

    if debug
      printf("f elemental + conds borde neumann elem %d\n", el)
      felem
    endif

    # matriz k de borde convectivo
    # k_lm = c*int(Nl*Nm dlin) = d(i-j)/3 si i==j o d(i-j)/6 si i!=j 
    # e.g. para el lado 31, kconv=c*d*[1/3 0 1/6 ; 0 0 0; 1/6 0 1/3]
    if mix(1,1)
      dist = sqrt((x2-x1)^2+(y2-y1)^2);
      kelem += mix(1,1)*dist*[1/3 1/6 0; 1/6 1/3 0; 0 0 0]; 
      felem += mix(1,1)*mix(1,2)*dist*[1/2;1/2;0];
    endif
    if mix(2,1)
      dist = sqrt((x1-x3)^2+(y1-y3)^2);
      kelem += mix(2,1)*dist*[1/3 0 1/6; 0 0 0, 1/6 0 1/3]; 
      felem += mix(2,1)*mix(2,2)*dist*[1/2;0;1/2];
    endif
    if mix(3,1)
      dist = sqrt((x3-x2)^2+(y3-y2)^2);
      kelem += mix(3,1)*dist*[0 0 0; 0 1/3 1/6; 0 1/6 1/3]; 
      felem += mix(3,1)*mix(3,2)*dist*[0;1/2;1/2];
    endif

    if debug
      printf("f elemental + conds borde mixtas elem %d\n", el)
      felem
    endif

    if debug
      printf("k elemental difusion + conds borde mixtas elem %d\n", el)
      kelem
    endif

    # ASSEMBLE
    for i=1:3
      for j=1:3
	Kg( elem(el,i),elem(el,j) ) += kelem(i,j);
      endfor
	fg( elem(el,i) ) += felem(i);
    endfor

  endfor

  ii = 1;
  for i=bcD(:,1)
    valor=bcD(ii,2);
    Kg(i,:) = eye(Nnodes)(i,:);
    fg(i) = valor;
    ii+=1;
  endfor

  T = Kg \ fg;
  
endfunction
