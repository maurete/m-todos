# función: heatfdm_1d_sta
# calcula la solución numérica a la ecuación del calor en 1D ESTÁTICA

# ecuación del calor: v dT/dx - k d²T/dx² + cT - g = 0
# donde v=v(x), k=k(x), c=c(x), g=g(x)

# TODO:
#        Agregar difusión numérica para salvar inestabilidad
#        Utilizar métodos de solución para matrices diagonales

function [ T x ]= heatfdm_1d_sta (
		    x,          # vector variable independiente [ 1xN ]
		    k=0,        # difusividad [ 1x1, 1xN ]
		    c=0,        # reactividad [ 1x1, 1xN ]
		    v=0,        # campo de velocidad [ 1x1, 1xN ]
		    g=0,        # término de producción [ 1x1, 1xN ]
		    bct0="d",   # tipo condicion en x(1) { d, n }
		    bcv0=1,     # valor de condicion en x(1) [ 1x1 ]
		    bct1="n",   # tipo condicion en x(N+1) { d, n }
		    bcv1=0      # valor de condicion en x(N+1) [ 1x1 ]
		  )


  # Calculo N, dx (@SIMPL asumo espaciado constante en x)
  N = size(x)(2);

  assert( size(x)(1)==1 );

  dx = ([x 0]-[0 x])(2:end);


  dx = x(2)-x(1);
  dx2 = dx^2;


  # Valido las dimensiones de c,g,k,v

  assert( size(c) == [1 1] || size(c) == [1 N] );
  assert( size(g) == [1 1] || size(g) == [1 N] );
  assert( size(k) == [1 1] || size(k) == [1 N] );
  assert( size(v) == [1 1] || size(v) == [1 N] );


  # genero vectores con los valores (iniciales) de g,c,k,v

  if size(g)(2) == 1
    f = ones([N+2 1]).*g;
  else
    f = [0; g(1,:)'; 0];
  endif
	 
  if size(c)(2) == 1
    ca = ones([1 N]).*c;
  else
    ca = c(1,:);
  endif
  
  if size(k)(2) == 1
    ka = ones([1 N]).*k;
  else
    ka = k(1,:);
  endif

  if size(v)(2) == 1
    va = ones([1 N]).*v;
  else
    va = v(1,:);
  endif


  # Verifico el Pèclet de la malla
  Pe = ( va ./ ka ) .* ( dx/2 );
  if max( abs( Pe ) ) >= 1
     fprintf ( "¡Cuidado: Pèclet_dx >= 1, solución inestable! (Pe_max = %f)\n", max(abs(Pe)) );
  endif


  # coeficientes auxliares (vectores en realidad)
  # con estos coef. definidos, la ecuacion a resolver queda:
  # T(l-1) * ( -A0(l) - A1(l) ) + T(l) * B(l) + T(l+1) * ( A0(l) - A1(l) )

  A0 = va ./ (2*dx);
  A1 = ka ./ dx2;
  B  = ka .* (2/dx2) + ca;

  K = zeros(N+2,N+2);

  # Indice "del problema"     -1  0  1  2  3  4  ...  N-2  N-1   N  N+1 ( N==n )
  # Indice en esta funcion     1  2  3  4  5  6  ...  N-1   N   N+1 N+2 (N==n+1)
  #                            ^--- nodos virtuales p/bc neumann ---^^^
 

  # lleno las filas interiores de la matriz
  for i=3:N
    K(i,i)=B(i-1);
    K(i,i-1)=-A0(i-1)-A1(i-1);
    K(i,i+1)=A0(i-1)-A1(i-1);
  endfor
  
  # condicion de borde en el extremo derecho
  switch bct1
  case { "d" "D" }
    # en caso de cond Dirichlet elimino las ultimas dos filas de f
    # y las dos ultimas filas y columnas de K
    f(N)=f(N)-bcv1*(A0(N)-A1(N));
    f=f(1:N);
    K=K(1:N,1:N);
  case { "n" "N" }
    # en caso Neumann lleno la anteultima fila de K como con las interiores
    # y agrego en la ultima la derivada: T(N+2)-T(N)/2*dx = val_x1
    K(N+1,N+1) = B(N);
    K(N+1,N) = -A0(N)-A1(N);
    K(N+1,N+2) = A0(N)-A1(N);
    K(N+2,N) = 1;
    K(N+2,N+2) = -1;
    f(N+2) = bcv1 * 2 * dx / ka(N);
  endswitch

  # condicion de borde en el extremo izquierdo
  switch bct0
  case { "n" "N" }
    # en caso Neumann lleno la segunda fila de K como con las interiores
    # y agrego en la primera la derivada: T(3)-T(1)/2*dx = val_x0
    K(1,1) = -1;
    K(1,3) = 1;
    K(2,2) = B(1);
    K(2,1) = -A0(1)-A1(1);
    K(2,3) = A0(1)-A1(1);
    f(1) = bcv0 * 2 * dx / ka(1);
  case { "d" "D" }
    # en caso de cond Dirichlet elimino las primeras dos filas de f
    # y las primeras dos filas y columnas de K
    f(3)=f(3) - bcv0*(-A0(1)-A1(1));
    f=f(3:end);
    K=K(3:end,3:end);
  endswitch

  # Resuelvo el sistema
  T = K\f;


  # Ajusto el tamaño final del vector solución T
  switch bct1
    case { "d" "D" }
      T = [ T; bcv1 ];
    case { "n" "N" }
      T = T(1:length(T)-1);
  endswitch
  switch bct0
    case { "d" "D" }
      T = [ bcv0; T ];
    case { "n" "N" }
      T = T(2:end);
  endswitch

endfunction
