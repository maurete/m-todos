# función: heatfdm_2d
# inicialmente supongo k, c, v constantes y estaticas, y g estática

# TODO: considerar espaciado no-uniforme en x,y
#       manejar k,c,vx,vy,g tensores definidos en cada punto del dominio
#       manejar condiciones de borde punto-a-punto, tipo y valor

function [ T n x y ]= heatfdm_2d (
		    x,          # vector variable independiente [ 1xL ]
		    y,          # vector variable independiente [ Mx1 ]
		    k=1,        # difusividad [ 1x1 o MxL ]
		    c=0,        # reactividad [ 1x1 o MxL ]
		    vx=0,       # comp x campo de velocidad [ 1x1 o MxL ]
		    vy=0,       # comp y campo de velocidad [ 1x1 o MxL ]
		    g=1,        # término de producción [ 1x1 o MxL ]
		    bctn="d",   # tipo condicion en y(1) { d, n }
		    bcvn=0,     # valor de condicion en y(1) [ 1x1 o 1xL ]
		    bcte="d",   # tipo condicion en x(L) { d, n }
		    bcve=0,     # valor de condicion en x(L) [ 1x1 o Mx1 ]
		    bctw="d",   # tipo condicion en x(1) { d, n }
		    bcvw=0,     # valor de condicion en x(1) [ 1x1 o Mx1 ]
		    bcts="d",   # tipo condicion en y(M) { d, n }
		    bcvs=0,     # valor de condicion en y(M) [ 1x1 o Mx1 ]
		    t=0,        # vector tiempo [ Nx1 o 1xN ]
		    T0=0,       # condición inicial [ 1x1 o MxL ]
		    z=0         # interpolacion del tiempo { 0=>explicito, 0.5=>C-N, 1=>bweuler}
		  )

  L = size(x)(2);
  M = size(y)(1);
  N = length(t);

  dx = (x(length(x))-x(1))/(length(x)); 
  dy = (y(length(y))-y(1))/(length(y));
  dt = (t(length(t))-t(1))/(length(t));

  dx2 = dx^2;
  dy2 = dy^2;

  Widx = 1;
  Eidx = L;
  Nidx = 1;
  Sidx = M;

  # genero matrices (vectores) g, k, c, v, bcv(n|e|w|s) a partir de los
  # parámetros donde todos tengan M filas (y L columas para g,k,c,v)
  switch size(g)
    case [1 1]
      ga = ones([M L]) .* g;
    case [M L]
      ga = g;
    otherwise
      error("El tamaño del vector g es inválido\n");
  endswitch
	 
  switch size(k)
    case [1 1]
      ka = ones([M L]) .* k;
    case [M L]
      ka = k;
    otherwise
      error("El tamaño del vector k es inválido\n");
  endswitch
  
  switch size(c)
    case [1 1]
      ca = ones([M L]) .* c;
    case [M L]
      ca = c;
    otherwise
      error("El tamaño del vector c es inválido\n");
  endswitch

  switch size(vx)
    case [1 1]
      vxa = ones([M L]) .* vx;
    case [M L]
      vxa = vx;
    otherwise
      error("El tamaño del vector vx es inválido\n");
  endswitch

  switch size(vy)
    case [1 1]
      vya = ones([M L]) .* vy;
    case [M L]
      vya = vy;
    otherwise
      error("El tamaño del vector vy es inválido\n");
  endswitch

  assert(size(bcvn)(1)==1,"Tamaño incorrecto de bcvn\n");
  assert(size(bcve)(2)==1,"Tamaño incorrecto de bcve\n");
  assert(size(bcvw)(2)==1,"Tamaño incorrecto de bcvw\n");
  assert(size(bcvs)(1)==1,"Tamaño incorrecto de bcvs\n");

  switch size(bcvn)(2)
    case 1
      bcvna = ones([1 L]) .*bcvn;
    case L
      bcvna = bcvn;
    otherwise
      error("El tamaño del vector bcvn es inválido\n");
  endswitch

  switch size(bcve)(1)
    case 1
      bcvea = ones([M 1]).*bcve;
    case M
      bcvea = bcve;
    otherwise
      error("El tamaño del vector bcve es inválido\n");
  endswitch

  switch size(bcvw)(1)
    case 1
      bcvwa = ones([M 1]).*bcvw;
    case M
      bcvwa = bcvw;
    otherwise
      error("El tamaño del vector bcvw es inválido\n");
  endswitch

  switch size(bcvs)(2)
    case 1
      bcvsa = ones([1 L]) .*bcvs;
    case L
      bcvsa = bcvs;
    otherwise
      error("El tamaño del vector bcvn es inválido\n");
  endswitch


  # máscara para la malla que indica cuáles nodos pertenecen al problema
  mask = ones ( [M+2 L+2] );
  mask(1,1)=0;
  mask(1,L+2)=0;
  mask(M+2,1)=0;
  mask(M+2,L+2)=0;
  
  # condicion de borde en el contorno norte (y_M)
  switch bctn
  case { "d" "D" }
    Nidx=2;
    mask(1,:)=0;
    mask(2,1)=0;
    mask(2,L+2)=0;
  case { "n" "N" }
    Nidx=1;
  endswitch

  # condicion de borde en el contorno este (x_L)
  switch bcte
  case { "d" "D" }
    Eidx=L+1;
    mask(:,L+2)=0;
    mask(1,L+1)=0;
    mask(M+2,L+1)=0;
  case { "n" "N" }
    Eidx=L+2;
  endswitch

  # condicion de borde en el contorno oeste (x_1)
  switch bctw
  case { "d" "D" }
    Widx=2;
    mask(:,1)=0;
    mask(1,2)=0;
    mask(M+2,2)=0;
  case { "n" "N" }
    Widx=1;
  endswitch

  # condicion de borde en el contorno sur (y_1)
  switch bcts
  case { "d" "D" }
    Sidx=M+1;
    mask(M+2,:)=0;
    mask(M+1,1)=0;
    mask(M+1,L+2)=0;
  case { "n" "N" }
    Sidx=M+2;
  endswitch

  # genero una matriz que indexa los nodos. cuento solo los que tienen mask=1
  [f n] = remap (mask,zeros([M+2 L+2]));

  # si solo tengo tiempo 0 resuelvo como estatica
  if N == 1
     T = heatfdm_2d_sta(x,y,k,c,vx,vy,g,bctn,bcvn,bcte,bcve,bctw,bcvw,bcts,bcvs);
     return
  endif

  len = length(f);

  # genero K y f del sistema a resolver. será de tamaño cont x cont
  K=zeros(len);
  #f=zeros([len 1]);

  # calculo los nodos de las fronteras
  switch bctn
  case { "d" "D" }
    for i=2:L+1
      K(n(2,i),n(2,i))=1;
      f(n(2,i))=bcvna(i-1);
    endfor
  case { "n" "N" }
    for i=2:L+1
      if mask(1,i)
	K(n(1,i),n(1,i))=1;
	K(n(1,i),n(3,i))=-1;
	f(n(1,i))= -2 * bcvna(i-1) * dy;
      endif
    endfor
  endswitch

  switch bcte
  case { "d" "D" }
    for i=2:M+1
      K(n(i,L+1),n(i,L+1))=1;
      f(n(i,L+1))=bcvea(i-1);
    endfor
  case { "n" "N" }
    for i=2:M+1
      if mask(i,L+2)
	K(n(i,L+2),n(i,L+2))=1;
	K(n(i,L+2),n(i,L))=-1;
	f(n(i,L+2))= -2 * bcvea(i-1) * dx;
      endif
    endfor
  endswitch

  switch bctw
  case { "d" "D" }
    for i=2:M+1
      K(n(i,2),n(i,2))=1;
      f(n(i,2))=bcvwa(i-1);
    endfor
  case { "n" "N" }
    for i=2:M+1
      if mask(i,1)
	K(n(i,1),n(i,1))=1;
	K(n(i,1),n(i,3))=-1;
	f(n(i,1))= 2 * bcvwa(i-1) * dx;
      endif
    endfor
  endswitch

  # condicion de borde en el contorno sur (y_1)
  switch bcts
  case { "d" "D" }
    for i=2:L+1
      K(n(M+1,i),n(M+1,i))=1;
      f(n(M+1,i))=bcvsa(i-1);
    endfor
  case { "n" "N" }
    for i=2:L+1
      if mask(M+2,i)
	K(n(M+2,i),n(M+2,i))=1;
	K(n(M+2,i),n(M,i))=-1;
	f(n(M+2,i))= 2 * bcvsa(i-1) * dy;
      endif
    endfor
  endswitch

  for i=Widx+1:Eidx-1
    for j=Nidx+1:Sidx-1
      K(n(j,i),n(j,i))   = 2*ka(j-1,i-1) * (1/dx2+1/dy2) + ca(j-1,i-1);
      K(n(j,i),n(j,i-1)) = - vxa(j-1,i-1) / (2*dx) - ka(j-1,i-1) / dx2;
      K(n(j,i),n(j,i+1)) =   vxa(j-1,i-1) / (2*dx) - ka(j-1,i-1) / dx2;
      K(n(j,i),n(j-1,i)) =   vya(j-1,i-1) / (2*dy) - ka(j-1,i-1) / dy2;
      K(n(j,i),n(j+1,i)) = - vya(j-1,i-1) / (2*dy) - ka(j-1,i-1) / dy2;
      f(n(j,i))          = ga(j-1,i-1);
    endfor
  endfor


  Pe = ( max(vxa,vya) ./ ka ) .* max ( [dx/2 dy/2] );
  Fo = ( ka ) .* ( dt/min([dx2 dy2]) );
  Co = ( max(vxa,vya) ) .* ( dt/min([dx dy]) );

  if max(max(abs(Pe))) >= 1
    warning ( "¡Pèclet_d(xy) >= 1, solución inestable! (Pe_max = %f)", max(max(abs(Pe))) );
  endif

  T = zeros([len N]);

  # Doy por hecho que T0 cumple las condiciones de borde
  switch size(T0)
    case [1 1]
      T(:,1) = remap( n, pad(ones([M L]).*T0,1,1,1,1));
    case [M L]
      T(:,1) = remap( n, pad(T0,1,1,1,1));
    otherwise
      error("El tamaño de T0 es inválido\n");
  endswitch

  assert( z>=0*z<=1, "valor de z fuera de rango (0<=z<=1)" );
  switch z
    case 0 # forward Euler

      if max(max(abs(Fo))) >= 1
	warning ( "¡Fourier >= 1, solución inestable! (Fo_max = %f)", max(max(abs(Fo))) );
      endif
      if max(max(abs(Co))) >= 1
	warning ( "¡Courant >= 1, solución inestable! (Co_max = %f)", max(max(abs(Co))) );
      endif

      for k=2:N
	T(:,k) = ( f - K*T(:,k-1) ) .* dt + T(:,k-1);
      endfor
    case 1 # backward Euler
      for k=2:N
	T(:,k) = (eye(len) + K.*dt) \ (f.*dt + T(:,k-1));
      endfor
    otherwise # Crank-Nicholson

      if max(max(abs(Fo))) >= z
	warning ( "¡Fourier >= %f, solución inestable! (Fo_max = %f)", z,max(max(abs(Fo))) );
      endif

      if max(max(abs(Co))) >= z
	warning ( "¡Courant >= %f, solución inestable! (Co_max = %f)", z,max(max(abs(Co))) );
      endif

      for k=2:N
	T(:,k) = (eye(len) + K.*(dt*z)) \ ( f.*dt + ( eye(len)-K.*(dt*(1-z)) ) * T(:,k-1));
      endfor

  endswitch


endfunction
