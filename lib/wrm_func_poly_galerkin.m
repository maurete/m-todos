# función wrm_func_poly_galerkin: aproxima la función "func" en el dominio
# dado por x, con polinómicas Nm=(1-x)*x^m, mediante el método de Galerkin.
# Devuelve la función aproximada y y los coeficientes para cada Nm
# en el vector a.

function [y a] = wrm_func_poly_galerkin ( func, x, M=5 )

  x0 = x(1);
  xL = x(length(x));

  # K,f los uso para armar el sistema Ka = f, con f=(fi-psi)
  K = zeros(M, M);
  f = zeros(M, 1);

  # función que satisface el contorno
  psi = x.*(feval(func,xL)-feval(func,x0))/(xL-x0)+feval(func,x0);

  faux = feval(func,x)-psi;

  # armo la matriz K y el vector f
  for l=1:M
    Nl = (-x+1).*x.^(l);
    for m=1:M
      Nm = (-x+1).*x.^(m);
      # trapz integracón numérica
      K(l,m) = trapz(x,Nm.*Nl);
    endfor
    f(l) = trapz(x,Nl.*faux);
  endfor

  # resuelvo para obtener las a
  a = K \ f;

  # y=y(x) es mi función aproximada. inicializo en cero y le sumo la psi
  y = psi;

  # sumo las am * Nm polinómicas
  for m=1:M
    y += (-x+1).*(x.^m).*a(m);
  endfor

endfunction
