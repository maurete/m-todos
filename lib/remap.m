# remap
# dada la mascara p de ceros y unos, mapea la matriz S en un vector columna R segun la matriz de correspondencia n
# dado el vector columna S, devuelve una matriz con los elementos ordenados según p.

function [ R n ] = remap( p, S )

  n=[];
  R=[];

  M = size(p)(1);
  L = size(p)(2);

  if size(S)(2)>1 && sum(sum(p==0+p==1))/(M*L)==1
     
    # debo mapear la matriz en un vector columna
    n = zeros( [M L] );
    cont = 0;
    R = zeros( [sum(sum(p==1)) 1] );

    if M<L
      for i=1:L
	if mod(i,2)
	  for j=M:-1:1
	    if p(j,i)
	      cont += 1;
	      n(j,i) = cont;
	      R(cont)=S(j,i);
	    endif
	  endfor
	else
	  for j=1:M
	    if p(j,i)
	      cont += 1;
	      n(j,i) = cont;
	      R(cont)=S(j,i);
	    endif
	  endfor
	endif
      endfor

    else
      for i=1:M
	if mod(i,2)
	  for j=1:L
	    if p(i,j)
	      cont += 1;
	      n(i,j) = cont;
	      R(cont)=S(i,j);
	    endif
	  endfor
	else
	  for j=L:-1:1
	    if p(i,j)
	      cont += 1;
	      n(i,j) = cont;
	      R(cont)=S(i,j);
	    endif
	  endfor
	endif
      endfor
    endif

    return;

  else
    #debo poner los elementos del vector columna S en el lugar especificado por p

    R = zeros ( [M L] );
    for j = 1:M
      for k = 1:L
	R(j,k) = S(p(j,k));
      endfor
    endfor

    n=p;
    return;

  endif
endfunction
