# función: ec_calor_cn
# calcula la solución numérica a la ecuación del calor en 1D, mediante un
# esquema de integración temporal Crank-Nicholson:
#  * si theta == 1   => implícito (BTCS),
#  * si theta == 0.5 => semi-implícito (O(dt^2))
#  * si theta == 0   => explícito (FTCS)

# ecuación del calor: dT/dt + v dT/dx - k d²T/dx² + cT - g = 0
# donde v=v(x,t), k=k(x,t), c=c(x,t), g=g(x,t)

function [ T x ]= heatfdm (
		    x,          # vector variable independiente [ 1xN ]
		    k=0,        # difusividad [ 1x1, 1xN o MxN ]
		    c=0,        # reactividad [ 1x1, 1xN o MxN ]
		    v=0,        # campo de velocidad [ 1x1, 1xN o MxN ]
		    g=0,        # término de producción [ 1x1, 1xN o MxN ]
		    bct0="d",   # tipo condicion en x(1) { d, n }
		    bcv0=1,     # valor de condicion en x(1) [ 1x1, 1xM ]
		    bct1="n",   # tipo condicion en x(N+1) { d, n }
		    bcv1=0,     # valor de condicion en x(N+1) [ 1x1, 1xM ]
		    t=0,        # vector tiempo discreto [ 0 o Mx1 ]
		    z=0.5,      # interpolacion del tiempo { 0=>explicito, 0.5=>C-N, 1=>bweuler}
		    T0=0        # temperatura inicial [ 1x1 o 1xN ]
		  )

  M = size(t)(1);
  N = size(x)(2);
  dt = (t(length(t))-t(1))/(length(t));
  dx = (x(length(x))-x(1))/(length(x));
  dx2 = dx^2;
  Lidx = 1;
  Ridx = N;

  if M == 1
     T = heatfdm_1d_sta(x,k,c,v,q,bct0,bcv0,bct1,bcv1);
     return
  endif

  # genero matrices (vectores) g, k, c, v, bcv0, bcv1 a partir de los
  # parámetros donde todos tengan M filas (y N columas para g,k,c,v)
  switch size(g)
    case [1 1]
      ga = ones([M N]) .* g;
    case [1 N]
      ga = zeros([M N]);
      for i=1:M
	ga(i,:) = g;
      endfor
    case [M 1]
      ga = zeros([M N]);
      for i=1:M
	ga(i,:) = ones([1 N]) .* g(i);
      endfor
    case [M N]
      ga = g;
    otherwise
      error("El tamaño del vector g es inválido\n");
  endswitch
	 
  switch size(k)
    case [1 1]
      ka = ones([M N]) .* k;
    case [1 N]
      ka = zeros([M N]);
      for i=1:M
	ka(i,:) = k;
      endfor
    case [M 1]
      ka = zeros([M N]);
      for i=1:M
	ka(i,:) = ones([1 N]) .* k(i);
      endfor
    case [M N]
      ka = k;
    otherwise
      error("El tamaño del vector k es inválido\n");
  endswitch
  
  switch size(c)
    case [1 1]
      ca = ones([M N]) .* c;
    case [1 N]
      ca = zeros([M N]);
      for i=1:M
	ca(i,:) = c;
      endfor
    case [M 1]
      ca = ones([M N]);
      for i=1:M
	ca(i,:) = ones([1 N]) .* c(i);
      endfor
    case [M N]
      ca = c;
    otherwise
      error("El tamaño del vector c es inválido\n");
  endswitch

  switch size(v)
    case [1 1]
      va = ones([M N]) .* v;
    case [1 N]
      va = zeros([M N]);
      for i=1:M
	va(i,:) = v;
      endfor
    case [M 1]
      va = zeros([M N]);
      for i=1:M
	va(i,:) = ones([1 N]) .* v(i);
      endfor
    case [M N]
      va = v;
    otherwise
      error("El tamaño del vector v es inválido\n");
  endswitch

  assert(size(bcv0)(2)==1,"Tamaño incorrecto de bcv0\n");
  assert(size(bcv1)(2)==1,"Tamaño incorrecto de bcv1\n");

  switch size(bcv0)(1)
    case 1
      bcv0a = ones([M 1]) .*bcv0;
    case M
      bcv0a = bcv0;
    otherwise
      error("El tamaño del vector bcv0 es inválido\n");
  endswitch

  switch size(bcv1)(1)
    case 1
      bcv1a = ones([M 1]).*bcv1;
    case M
      bcv1a = bcv1;
    otherwise
      error("El tamaño del vector bcv1 es inválido\n");
  endswitch

  # Genero T, que será de MxN, llenando la primera fila con T0
  T = zeros([M N+2]);

  assert(size(T0)(1)==1);
  switch size(T0)(2)
    case 1
      T(1,2:N+1) = ones([1 N]).*T0;
    case N
      T(1,2:N+1) = T0;
    otherwise
      error("El tamaño del vector T0 es inválido\n");
  endswitch

  Fo = ( ka ) .* ( dt/dx2 );
  Co = ( va ) .* ( dt/dx );
  Pe = ( va ./ ka ) .* ( dx/2 );

  if max(max(abs(Pe))) >= 1
    warning ( "¡Pèclet_dx >= 1, solución inestable! (Pe_max = %f)", max(max(abs(Pe))) );
  endif

  # condicion de borde en el extremo izquierdo
  switch bct0
  case { "d" "D" }
    Lidx=2;
    if ( T(1,2) != bcv0a(1) )
       warning( "Apa, T0 no cumple la cond. de contorno en el extremo izquierdo." );
    endif
  case { "n" "N" }
    Lidx=1;
    # Asumo que la cond. de borde en x=0 viene dada por dT/dx=q/k,
    # esto es, bcv0 = q/k
    T(1,1) = T(1,3) - 2 * dx * bcv0a(1);
  endswitch

  # condicion de borde en el extremo derecho
  switch bct1
  case { "d" "D" }
    Ridx=N+1;
    if ( T(1,N+1) != bcv1a(1) )
       warning( "Apa, T0 no cumple la cond. de contorno en el extremo derecho." );
    endif
  case { "n" "N" }
    Ridx=N+2;
    # Asumo que la cond. de borde en x=L viene dada por dT/dx=-q/k,
    # esto es, bcv1 = -q/k
    T(1,N+2) = T(1,N) - 2 * dx * bcv1a(1);
  endswitch

  #fprintf("Lidx: %i, Ridx:%i, length(T): %i\n", Lidx, Ridx,size(T)(2))

  # Resuelvo a partir de T(2)
  switch z
    # Esquema explícito: forward Euler
    case 0
      #disp("Esquema explícito");
      if max(max(abs(Fo))) >= 1
	warning ( "¡Fourier >= 1, solución inestable! (Fo_max = %f)", max(max(abs(Fo))) );
      endif
      if max(max(abs(Co))) >= 1
	warning ( "¡Courant >= 1, solución inestable! (Co_max = %f)", max(max(abs(Co))) );
      endif

      # Itero en el tiempo
      for m=2:M
	# Itero en el espacio
	for i=Lidx+1:Ridx-1
	    T(m,i) = ga(m-1,i-1)*dt \
	             + T(m-1,i-1)*dt*(va(m-1,i-1)/(2*dx)+ka(m-1,i-1)/dx2) \
		     + T(m-1,i)*(1-dt*(ca(m-1,i-1)+ka(m-1,i-1)*2/dx2)) \
		     - T(m-1,i+1)*dt*(va(m-1,i-1)/(2*dx)-ka(m-1,i-1)/dx2);
	endfor
	# Calculo los extremos izq y derecho
	switch bct0
	  case { "d" "D" }
	    T(m,2) = bcv0a(m); 
	  case { "n" "N" }
	    T(m,1) = T(m,3) - 2 * dx * bcv0a(m);
	endswitch
	switch bct1
	  case { "d" "D" }
	    T(m,Ridx) = bcv1a(m);
	  case { "n" "N" }
	    T(m,Ridx) = T(m,N) - 2 * dx * bcv1a(m);
	endswitch
      endfor

    # Esquema implícito: backward Euler
    case 1
      for m=2:M
	K = zeros(N+2,N+2);
	f = zeros(N+2,1);
	
	for i=Lidx+1:Ridx-1
	  K(i,i-1) = -dt*(va(m,i-1)/(2*dx)+ka(m,i-1)/dx2);
	  K(i,i+1) = dt*(va(m,i-1)/(2*dx)-ka(m,i-1)/dx2);
	  K(i,i) = 1+dt*(ca(m,i-1)+ka(m,i-1)*2/dx2);
	  f(i) = dt*ga(m,i-1)+T(m-1,i);
	endfor
	# Calculo los extremos izq y derecho
	switch bct0
	  case { "d" "D" }
	    f(2) = bcv0a(m);
	    K(2,2) = 1;
	  case { "n" "N" }
	    K(1,1) = -1;
	    K(1,3) = 1;
	    f(1) = 2 * dx * bcv0a(m);
	endswitch
	switch bct1
	  case { "d" "D" }
	    f(Ridx) = bcv1a(m);
	    K(Ridx,Ridx) = 1;
	  case { "n" "N" }
	    K(Ridx,Ridx) = 1;
	    K(Ridx,Ridx-2) = -1;
	    f(Ridx) = -2 * dx * bcv1a(m);
	endswitch	
	T(m,Lidx:Ridx) = ( K(Lidx:Ridx,Lidx:Ridx) \ f(Lidx:Ridx) )';
      endfor
    otherwise
      assert( z>0 && z<1, "¡z debe valer entre 0 y 1!\n");
      if max(max(abs(Fo))) >= 0.5
	warning ( "¡Fourier >= 1/2, solución inestable! (Fo_max = %f)", max(max(abs(Fo))) );
      endif

      for m=2:M
	K = zeros(N+2,N+2);
	f = zeros(N+2,1);
	zv_2dx = va(m,:).*(z/(2*dx));
	zk_dx2 = ka(m,:).*(z/dx2);
	dti = 1/dt;
	zm = 1-z;
	zmv0_2dx = va(m-1,:).*((1-z)/(2*dx));
	zmk0_dx2 = ka(m-1,:).*((1-z)/dx2);
	for i=Lidx+1:Ridx-1
	  K(i,i-1) = -zv_2dx(i-1)-zk_dx2(i-1);
	  K(i,i+1) =  zv_2dx(i-1)-zk_dx2(i-1);
	  K(i,i) = dti+z*ca(m,i-1)+2*zk_dx2(i-1);
	  f(i) = z*ga(m,i-1) + zm*ga(m-1,i-1) \
		 - T(m-1,i+1) * ( zmv0_2dx(i-1) - zmk0_dx2(i-1) ) \
		 + T(m-1,i) * ( dti - zm*ca(m-1,i-1) - 2*zmk0_dx2(i-1) ) \
		 + T(m-1,i-1) * ( zmv0_2dx(i-1) + zmk0_dx2(i-1) );
	endfor
	# Calculo los extremos izq y derecho
	switch bct0
	  case { "d" "D" }
	    f(2) = bcv0a(m);
	    K(2,2) = 1;
	  case { "n" "N" }
	    K(1,1) = -1;
	    K(1,3) = 1;
	    f(1) = 2 * dx * bcv0a(m);
	endswitch
	switch bct1
	  case { "d" "D" }
	    f(Ridx) = bcv1a(m);
	    K(Ridx,Ridx) = 1;
	  case { "n" "N" }
	    K(Ridx,Ridx) = 1;
	    K(Ridx,Ridx-2) = -1;
	    f(Ridx) = -2 * dx * bcv1a(m);
	endswitch
	#tipo = matrix_type( K(Lidx:Ridx,Lidx:Ridx) )
	T(m,Lidx:Ridx) = ( K(Lidx:Ridx,Lidx:Ridx) \ f(Lidx:Ridx) )';
      endfor
  endswitch
  T = T(:,2:N+1);
endfunction


## octave:1> x=0:1/40:1;
## octave:2> t=[0:1/200:1]';
## octave:3> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0.5,1); mesh(T)
## octave:4> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,1,1); mesh(T)
## octave:5> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,1,sin(10*x)); mesh(T)
## octave:6> T=heatfdm(x,0.01,1,0.2,0,"n",0,"n",0,t,1,sin(10*x)); mesh(T)
## octave:7> T=heatfdm(x,0.01,4,0.2,0,"n",0,"n",0,t,1,sin(10*x)); mesh(T)
## octave:8> T=heatfdm(x,0.1,1,0.2,0,"n",0,"n",0,t,1,sin(10*x)); mesh(T)
## octave:9> T=heatfdm(x,0.1,1,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:10> T=heatfdm(x,1,1,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## warning: ¡Fourier >= 1, solución inestable! (Fo_max = 8.363184)
## octave:11> T=heatfdm(x,0.01,1,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:12> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:13> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:14> t=[0:1/400:1]';
## octave:15> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:16> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## ^C
## octave:16> t=[0:1/400:0.5]';
## octave:17> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:18> t=[0:1/800:0.25]';
## octave:19> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:20> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## ^C
## octave:20> t=[0:1/1600:0.125]';
## octave:21> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:22> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## ^C
## octave:22> t=[0:1/3200:0.0675]';
## octave:23> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,sin(10*x)); mesh(T)
## octave:24> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,cos(10*x)); mesh(T)
## octave:25> t=[0:1/800:0.25]';
## octave:26> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0,cos(10*x)); mesh(T)
## octave:27> T=heatfdm(x,0.01,0.5,0.2,0,"d",0,"d",0,t,0,cos(10*x)); mesh(T)
## warning: Apa, T0 no cumple la cond. de contorno en el extremo izquierdo.
## warning: Apa, T0 no cumple la cond. de contorno en el extremo derecho.
## octave:28> T=heatfdm(x,0.01,0.5,0.2,0,"d",0,"d",0,t,1,cos(10*x)); mesh(T)
## warning: Apa, T0 no cumple la cond. de contorno en el extremo izquierdo.
## warning: Apa, T0 no cumple la cond. de contorno en el extremo derecho.
## octave:29> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,1,cos(10*x)); mesh(T)
## octave:30> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,1,sin(10*x)); mesh(T)
## octave:31> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,1,sin(10*pi*x)); mesh(T)
## octave:32> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0.5,sin(10*pi*x)); mesh(T)
## octave:33> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0.8,sin(10*pi*x)); mesh(T)
## octave:34> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,0.9,sin(10*pi*x)); mesh(T)
## octave:35> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,1,sin(10*pi*x)); mesh(T)
## octave:36> T=heatfdm(x,0.01,0.5,0.2,0,"n",0,"n",0,t,1,sin(10*pi*x)); mesh(T)
