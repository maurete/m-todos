# función: heatfem_1d_sta
# calcula la solución numérica a la ecuación del calor en 1D ESTÁTICA
# por el m'etodo de los elementos finitos LINEALES

# ecuación del calor: v dT/dx - k d²T/dx² + cT - g = 0
# donde v=v(x), k=k(x), c=c(x), g=g(x)

function [ T x ]= heatfem_1d_sta (
		    xnod,       # vector de nodos x [ 1xM ]
		    k=0,        # difusividad [ 1x1, 1xM ]
		    c=0,        # reactividad [ 1x1, 1xM ]
		    v=0,        # campo de velocidad [ 1x1, 1xM ]
		    g=0,        # término de producción [ 1x1, 1xM ]
		    bct0="d",   # tipo condicion en x(1) { d, n }
		    bcv0=1,     # valor de condicion en x(1) [ 1x1 ]
		    bct1="n",   # tipo condicion en x(M) { d, n }
		    bcv1=0      # valor de condicion en x(M) [ 1x1 ]
		  )

  M = size(xnod)(2);

  assert( size(xnod)(1) == 1, "xnod debe ser un vector fila" );
  assert( size(c) == [1 1] || size(c) == [1 M], "error en el tamaño de c" );
  assert( size(g) == [1 1] || size(g) == [1 M], "error en el tamaño de g" );
  assert( size(k) == [1 1] || size(k) == [1 M], "error en el tamaño de k" );
  assert( size(v) == [1 1] || size(v) == [1 M], "error en el tamaño de v" );

  # obtengo el orden de los nodos: para el elemento e, los nodos
  # correspondientes serán { order(e), order(e+1) }
  [ order order ] = sort ( xnod );

  # genero vectores con los valores (iniciales) de g,c,k,v
  ga = ones([1 M]).*g;
  ca = ones([1 M]).*c;
  ka = ones([1 M]).*k;
  va = ones([1 M]).*v;

  Kg = zeros([M M]);
  fg = zeros([M 1]);

  % para cada elemento, considero los nodos como interiores
  for el=1:M-1
    i1_ = order(el);
    i2_ = order(el+1);
    he = diff( xnod([i1_ i2_]) );

    # no necesito usar integración numérica ya que tengo el resultado calculado
    [Ke fe] = heat_1d_sta_fem_elem( he,
				    ka([i1_ i2_]),
				    ca([i1_ i2_]),
				    va([i1_ i2_]),
				    ga([i1_ i2_]) );

    # Sumo la matriz elemental Ke a la pos. corresp. en Kg
    Kg(i1_,i1_) += Ke(1,1);
    Kg(i1_,i2_) += Ke(1,2);
    Kg(i2_,i1_) += Ke(2,1);
    Kg(i2_,i2_) += Ke(2,2);

    # Agrego en el RHS la contrib elemental
    fg(i1_,1) += fe(1,1);
    fg(i2_,1) += fe(2,1);
  endfor

  # evalúo los contornos
  switch bct0
    case { "d" "D" }
      # si es Dirichlet piso la primer file de Kg con [1 0 0 ...]
      Kg(1,:) = eye(M)(1,:);
      fg(1) = bcv0;      
    case {"n" "N"}
      # si es Neumann pongo el valor en el RHS
      fg(1) += bcv0;
  endswitch

  switch bct1
    case { "d" "D" }
      Kg(M,:) = eye(M)(M,:);
      fg(M) = bcv1;
    case {"n" "N"}
      fg(M) += bcv1;
  endswitch

  T = Kg \ fg;
  
  x = xnod;

endfunction
