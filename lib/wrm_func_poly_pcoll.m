# función wrm_func_poly_pcoll: aproxima la función "func" en el dominio
# dado por x, con polinómicas Nm=(1-x)*x^m, mediante colocación puntual
# equiespaciada en el interior del dominio.
# Devuelve la función aproximada y y los coeficientes mara cada Nm
# en el vector a.

function [y a] = wrm_func_poly_pcoll ( func, x, M=5 )

  # extremos del dominio x0 y xL
  x0 = x(1);
  xL = x(end);

  # xd contiene los M-1 puntos interiores donde se evalúa la función
  # (el contorno lo satisface psi)
  xd = [x0:1/(M+1):xL](2:M+1); # descarto x0 y xL

  # K,f los uso para armar el sistema Ka = f, con f=(fi-psi)
  K = zeros(M, M);
  f = zeros(M, 1);

  # función que satisface el contorno, pero aca la evalúo sólo en el interior
  # psi(x) = f(x0) + ( f(xL)-f(x0)/xL-x0) )
  # es una recta que va de (x0,f(x0)) a (xL,f(xL))
  psi = xd.*(feval(func,xL)-feval(func,x0))/(xL-x0)+feval(func,x0);

  # armo la matriz K y el vector f
  for l=1:M
    for m=1:M
      # como uso colocacion puntual, tengo Klm = Nm(xl)
      K(l,m) = (1-xd(l))*xd(l)^(m);
    endfor
    # el RHS es igual a f(x)-psi(x) evaluada en x=xl
    f(l) = feval(func,xd(l)) - psi(l);
  endfor

  # resuelvo para obtener los coeficientes a
  # (recordar la aproximacion: psi + sum( a_m*N_m )
  a = K \ f;

  # y=y(x) es mi función aproximada. inicializo en cero y le sumo la psi
  y = zeros(size(x));
  y += x.*(feval(func,xL)-feval(func,x0))/(xL-x0)+feval(func,x0);

  # sumo sucesivamente las am * Nm polinómicas
  for m=1:M
    y += (-x+1).*(x.^m).*a(m);
  endfor

endfunction
