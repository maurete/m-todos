# función heat_1d_sta_fem_elem: calcula la matriz elemental para
# la ecuación del calor general k d²T/dx² - v dT/dx + cT - g = 0
#
# En el cálculo de los elementos de la matriz elemental se trató a las
# cantidades k,c,v,g como constantes (i.e. invariables en x, mientras
# que en esta función hacemos el "truco" de asignar en los terminos
# que multiplican al nodo \phi_1 el valor de kcvg en x_1 y los que
# multiplican el nodo 2 el valor de kcvg en x_2.  Esto no tiene
# sutento matemático probado, pero teniendo en cuenta que estamos
# considerando un scope local, tampoco es tan tirado de los pelos.

function [ Ke fe ]= heat_1d_sta_fem_elem (
		  he,        # ancho del elemento
		  k=0,       # difusividad
		  c=0,       # reactividad
		  v=0,       # campo de velocidad
		  g=0        # término de producción
		)

  assert(size(k)(1)==1,"tamaño incorrecto de k");
  assert(size(c)(1)==1,"tamaño incorrecto de c");
  assert(size(v)(1)==1,"tamaño incorrecto de v");
  assert(size(g)(1)==1,"tamaño incorrecto de g");

  ka = [1 1] .* reshape(k,1,:);
  ca = [1 1] .* reshape(c,1,:);
  va = [1 1] .* reshape(v,1,:);
  ga = [1 1] .* reshape(g,1,:);

  Ke = [];
  fe = [];

  Ke(1,1) =   ka(1)/he - va(1)/2 + ca(1)*he/3 ;
  Ke(1,2) = - ka(2)/he + va(2)/2 + ca(2)*he/6 ;
  Ke(2,1) = - ka(1)/he - va(1)/2 + ca(1)*he/6 ;
  Ke(2,2) =   ka(2)/he + va(2)/2 + ca(2)*he/3 ;

  fe(1,1) = ga(1)*he/2;
  fe(2,1) = ga(2)*he/2;

endfunction
