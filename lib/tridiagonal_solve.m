#!/usr/bin/env octave -qf
#
# tridiagonal_solve
# resuelve la matriz tridiagonal según el algoritmo para matrices tridiagonales
# (Thomas) de o(n). Ver http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
# 
function x = tridiagonal_solve ( K, f )
  n = length(f);
 
  # Modify the first-row coefficients
  K(1,2) = K(1,2) / K(1,1); # Division by zero risk.
  f(1) = f(1) / K(1,1);     # Division by zero would imply a singular matrix. 
 
  for i = 2:n-1
    id = 1 / (K(i,1) - K(i-1,i) * K(i,i-1)); # Division by zero risk. 
    K(i,i+1) = K(i,i+1)* id;                 # Last value calculated is redundant.
    f(i) = (f(i) - f(i-1) * K(i,i-1)) * id;
  endfor
  
  # Now back substitute.
  x(n) = f(n);
  for i = n-1:-1:1
    x(i) = f(i) - K(i,i+1) * x(i+1);
  endfor
endfunction
