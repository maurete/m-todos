\documentclass[a4paper,11pt,oneside]{article}
%
% importar el archivo conf/packages.tex
\include{conf/packages}
%
% ===
% === Propiedades del documento: título, autor, etc
% ===
%
\newcommand{\titulo}{{\large FICH --- UNL}\\Métodos Numéricos y Simulación
                     -- 2010\\{Guía de Trabajos Prácticos 1}}
\newcommand{\autor}{Torrez, Mauro}
\newcommand{\fecha}{\today}
\newcommand{\tituloPDF}{MNS GTP1}
\newcommand{\autorPDF}{Mauro Torrez}
\newcommand{\asuntoPDF}{MNS GTP1}
\newcommand{\clavesPDF}{MNS GTP1}
%
% importar los archivos conf/config.tex y conf/comandos.tex
\include{conf/config}
\include{conf/comandos}
\include{conf/fuentes}
%
% ===
% === Inicio del documento
% ===
%
\begin{document}
% crear la página de título
\maketitle
%
\section*{Ejercicio 1}
%
Dado el problema de conducción del calor en 1D
\begin{align*}
  \dpar{T}{t}{} + k\Delta T + Q -c(T-T_\T{amb})=0, \quad 0 \leq x \leq L
\end{align*}
considero el caso estacionario, con $c=0$ y $k=1$:
\begin{align*}
  \dx{T}{2} =-Q, \quad 0 \leq x \leq L
\end{align*}
\subsection*{Cálculo de la solución exacta}
Como $Q$ viene definida constante de a trozos
\begin{equation*}
  Q(x)=
  \begin{cases}
  1, & 0\leq x\leq\frac{1}{2}, \\ 0, & \frac{1}{2}< x\leq 1
  \end{cases}
\end{equation*}
debo resolver simultáneamente los problemas de valor en el contorno
\begin{align*}
  \begin{cases}
    \dx{T_1}{2} =-1, & 0 \leq x \leq\frac{1}{2},\\
    T_1(x=0)=1
  \end{cases}
&&\begin{cases}
    \dx{T_2}{2} =0, & \frac{1}{2} < x \leq 1,\\
    T_2(x=1)=0
  \end{cases}
\end{align*}
agregando las siguientes restricciones:
\begin{align*}
  T_1(x=1/2)=T_2(x=1/2), &&
  \dx{T_1}{}(x=1/2)=\dx{T_2}{}(x=1/2)
\end{align*}
ya que el flujo es una función continua.

Integrando directamente $T_1$ y $T_2$, obtengo
\begin{align*}
  T_1=\frac{x^2}{2}+c_1 x +d_1, && T_2=c_2 x+d_2.
\end{align*}
Considerando las condiciones dadas en los extremos y las dos restricciones
agregadas al separar el problema en dos, tengo finalmente
\begin{align*}
  T_1=-\frac{x^2}{2}-\frac{5}{8} x+1, && T_2=\frac{9}{8} (-x+1).
\end{align*}
Así que la solución exacta al problema dado es
\begin{equation*}
  T(x)=
  \begin{cases}
    -\frac{1}{2}x^2-\frac{5}{8} x+1, & 0 \leq x \leq\frac{1}{2},\\
    \frac{9}{8} (-x+1), & \frac{1}{2} < x \leq 1.
  \end{cases}
\end{equation*}
%
\subsection*{Solución numérica}
En este caso la solución numérica se obtiene resolviendo un sistema de ecuaciones
$\mathbf{K T = f}$ donde la matriz $\mathbf{K}$ se obtiene a partir del stencil
de diferencias finitas
\begin{align*}
  \evalen{i}{\dx{T}{2}} \approx \frac{T_{i+1}-2T_i+T_{i+1}}{\Delta x^2}
\end{align*}
evaluando $T$ en los nodos interiores.
%
%\aclaracion{\small\emph{Función de Octave}}
           {\small\verbatiminput{src/solve_fdm_1d.m}}
En la figura \ref{fig:ej1} podemos ver cómo el error disminuye al
aumentar el número de segmentos $N$ en el método de diferencias
finitas.  Ver en el apéndice \ref{ap:ej1} las funciones utilizadas
para generar la gráfica.
\begin{figure}[h]
  {\scriptsize\include{img/ej1}}
  \caption{Error $E$ de la aproximación para distintos números de
    segmentos $N$.}
  \label{fig:ej1}
\end{figure}
%
%
%
\newpage
\section*{Ejercicio 2}
\subsection*{Solución exacta}
En este caso en vez de calcular analíticamente la solución exacta, la
estimamos mediante extrapolación de Richardson: usando dos
aproximaciones numéricas $\phi_1$ y $\phi_2$, con espaciado de malla
$\Delta x_1$ y $\Delta x_2$ respectivamente, se cumple la relación
\begin{align*}
  \frac{\phi^e - \phi^1}{\phi^e - \phi^2} = \left(\frac{\Delta
    x_1}{\Delta x_2}\right)^2
\end{align*}
donde $\phi^e_l$ es el ``orden'' de la solución exacta.
Despejando $\phi^e$, tengo
\begin{align*}
  \phi^e = \frac{\phi^1 - \left(\frac{\Delta
    x_1}{\Delta x_2}\right)^2 \phi^2}{1 - \left(\frac{\Delta
    x_1}{\Delta x_2}\right)^2}
\end{align*}
y uso esta estimación para calcular el error cometido al resolver
el problema con diferencias finitas.
\subsection*{Elección del paso de tiempo $\Delta t$}
El esquema de integración temporal explícita produce soluciones estables sólo
si se cumple la condición de Fourier:
\begin{align*}
  \Gamma  = \frac{k\Delta t}{\Delta x^2} < \frac{1}{2}
\end{align*}
Como para este ejercicio usaré un $\Delta x=0.2$ y $k=1$, el valor de
$\Delta t$ estará acotado por
\begin{align*}
  \Delta t  < \frac{0.2^2}{2} = 0.02.
\end{align*}
\subsection*{Solución numérica}
El algoritmo de solución numérica consistirá principalmente en calcular el
valor de $T$ para cada nodo $i$ en el instante $m$ utilizando el stencil
\begin{align*}
  T_i^m = (1-2\Gamma)T_i^{m-1}+\Gamma T_{i-1}^{m-1}+\Gamma T_{i+1}^{m-1}
  -\Delta t Q_i -\Delta t c(T_i^{m-1}-T_{amb})
\end{align*}
donde el $T$ inicial es dato (si no, se supone constante e igual a la
temperatura ambiente) y la condición de contorno Neumann se calcula
utilizando un nodo ficticio.

Cabe notar que en este caso, no es necesario resolver ningún sistema de
ecuaciones en todo el proceso.

\newpage
\aclaracion{\small\emph{ec\_calor\_guia.m}}
{\small\verbatiminput{src/ec_calor_guia.m}}

En la figura \ref{fig:ej2} vemos una gráfica del error de aproximación $E$
para distintos pasos de tiempo $\Delta t$ en distintos instantes $t_1$ y $t_2$.
%
\begin{figure}[h]
  {\scriptsize\include{img/ej2}}
  \caption{Error $E$ de la aproximación en $t_1=0.2s$ y $t_2=1.5s$, para distintos
    valores de $\Delta t$.}
  \label{fig:ej2}
\end{figure}
%
%
%
\section*{Ejercicio 3}
Para este ejercicio aproximamos el orden de la solución exacta del
mismo modo que en el caso anterior. El método de Crank-Nicholson es
centrado en el tiempo, evaluando para cada instante $t_i$ la función
en $t_i-\frac{\Delta t}{2}$ y $t_i+\frac{\Delta t}{2}$, de esta manera el error al
aproximar la derivada temporal pasa a ser $\mathcal{O}(\Delta
t^2)$. El stencil para este esquema es
\begin{align*}
  BT_{i-1}^m + AT_{i-1}^m+ BT_{i-1}^m &= DT_{i-1}^{m-1} +
  CT_{i-1}^{m-1}+ DT_{i-1}^{m-1}+c*T_{amb}+Q_i,
\end{align*}
con
\begin{align*}
  A = 1+\theta(2\Gamma+c), && B = -\theta\Gamma, && C =
  (\theta-1)(2\Gamma+c), && \T{y}&& D = \Gamma(1-\theta).
\end{align*}
%
Esto conduce a un sistema de ecuaciones $\mathbf{KT=f}$, donde
$\mathbf{K}$ es tridiagonal y $\mathbf{f}$ contiene el lado derecho de
la ecuación presentada arriba.

El método de Crank-Nicholson utiliza $\theta=\frac{1}{2}$, lo cual
implica que es marginalmente estable y con orden de convergencia
$\mathcal{O}(\Delta t^2)$. Haciendo $\theta>\frac{1}{2}$ ganamos en
estabilidad (pasa a ser incondicionalente estable), sacrificando la
convergencia cuadrática.

\aclaracion{\small\emph{ec\_calor\_cn.m}}
{\small\verbatiminput{src/ec_calor_cn.m}}

\begin{figure}[h]
  {\scriptsize\include{img/ej3}}
  \caption{Error $E$ de la aproximación para $t_1=0.2s$, $t_2=1.5s$,
    para distintos valores de $\Delta t$.}
  \label{fig:ej3}
\end{figure}
\appendix
\section{Apéndice ej. 1}\label{ap:ej1}
\aclaracion{\small\emph{ej1\_exacta.m}}
{\small\verbatiminput{src/ej1_exacta.m}}

\aclaracion{\small\emph{error\_ej1.m}}
{\small\verbatiminput{src/error_ej1.m}}

\aclaracion{\small\emph{ej1.m}}
{\small\verbatiminput{src/ej1.m}}
\newpage
\section{Apéndice ej. 2}\label{ap:ej2}
%\aclaracion{\small\emph{ej2.m}}
{\small\verbatiminput{src/ej2.m}}

\section{Apéndice ej. 3}\label{ap:ej3}
%\aclaracion{\small\emph{ej3.m}}
{\small\verbatiminput{src/ej3.m}}
\end{document}
