function [T x t F]= solve_heat_1d_fdm_bweuler( k=1, c=0, Tenv=0, f_Q="", N=20,
					   dt=0.1, N_t=20, Tini="",
					   x0=0, cond_T0="d", val_T0=1, 
		                           x1=1, cond_T1="n", val_T1=0)
  # Genero el dominio de la var. indep. x
  x = x0:1/N:x1;
  dx = x(2)-x(1);
  dx2 = dx^2;

  # Genero el T inicial
  if length(Tini) > 0
    T = feval( Tini, x0, x1, N)';
  else
    T = zeros( size(x))'; #sin( x*pi)';
  endif

  # Calculo el término final que no depende de x:
  # F_i = delta_t * ( Q_i - T_ambiente )
  if length(f_Q) > 0
    F = feval( f_Q, x0, x1, N)';
  else
    F = zeros( size(x))';
  endif
  F += c*Tenv;
  F *= dt;

  # En este punto ya tengo T en el tiempo 0.

  # Calculo coeficiente auxiliar que multiplica a fi_i
  coef_i = 1+2*k*dt/dx2+c*dt;
  # coef auxiliar que miltiplica a fi_i+1 y fi_i-1
  gamma = k*dt/dx2;

  # itero en el tiempo: FTCS
  for m=2:N_t
    
    # inicializo un nuevo T para el tiempo m*dt
    T = [ T zeros( N+1, 1) ];
    
    # considero la condición de borde en x0
    if cond_T0=="d"
      T(1,m) = val_T0;
    elseif cond_T0=="n"
      # calculo el punto ficticio fi_-1 en la grilla en el tiempo anterior
      # éste viene dado de despejar (dfi1 - dfi-1)/dx = val_T0
      fict = T(2,m-1) - dx*val_T0;
      # ahora calculo fi_0 como cualquier otro punto interior
      T(1,m) = T(1,m-1)*coef_i - gamma*(fict + T(2,m-1)) - F(1);
    endif

    # itero en los puntos interiores
    for i=2:N
      T(i,m) = T(i,m-1)*coef_i - gamma*(T(i-1,m-1) + T(i+1,m-1)) - F(i);
    endfor

    # considero la condición de borde en xL
    if cond_T1=="d" | cond_T1=="D"

      T(N+1,m) = val_T1;
    elseif cond_T1=="n" | cond_T1=="N"

      fict = T(N,m-1) + dx*val_T1;

      T(N+1,m) = T(N+1,m-1)*coef_i - gamma*(T(N,m-1) + fict) - F(N+1);
    endif

  endfor

  t = dt:dt:N_t*dt;

endfunction