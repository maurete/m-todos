#!/usr/bin/octave3.0 -qf
function ej2
  set(0, "defaultlinelinewidth", 2);
  figure

  # defino x
  x=0:1/5:1;

  # defino t1 y t2, para calcular el orden de la soln exacta
  t1=0:1/100:2;
  t2=0:1/200:2;

  # calculo para t1, t2
  T1=ec_calor_guia(x,t1,1,1,0,"Q_ej1b","","d",1,"n",0);
  T2=ec_calor_guia(x,t2,1,1,0,"Q_ej1b","","d",1,"n",0);

  # obtengo la T "exacta" mediante extrapolación de Richardson
  c=(200/100)^2;
  for i=1:201
    Te(i,:) = (T1(i,:)-(T2((2*i-1),:)*c))/(1-c);
  endfor

  # calculo T para distintos dt, guardo los valores en los
  # instantes t=0.2 y t=1.5 segundos
  dts=[1/50 1/60 1/70 1/80 1/100 1/120 1/150 1/200 1/280 1/300 1/320 1/400 1/500];
  e1= zeros(1,length(dts));
  e2= zeros(1,length(dts));

  for i=1:length(dts)
    dt=dts(i);
    t=0:dt:2;
    T=ec_calor_guia(x,t,1,1,0,"Q_ej1b","","d",1,"n",0);

    # guardo el error en t=0.2 y 1.5s
    e1(i) = trapz(x,abs(Te(21,:)-T(1+round(0.2/dt),:)));
    e2(i) = trapz(x,abs(Te(151,:)-T(1+round(1.5/dt),:)));
    # e1f(i) = trapz(x,abs(Tf-T(1+1/dt,:)));
    # e2f(i) = trapz(x,abs(Tf-T(1+2/dt,:)));
  endfor

  loglog( dts, e1,"-r;$E(T|_{t=0.2})$;",dts, e2,"-b;$E(T|_{t=1.5})$;");
  xlabel("$\\Delta t$");
  ylabel("$E$");

  print -dtex "img/ej2.tex";
  print -depsc2 "img/ej2eps.eps";
  shell_cmd("cd img; epstopdf ej2.eps");
  shell_cmd("cd img; epstopdf ej2eps.eps");

endfunction
#
if strcmp(program_name(), "ej2.m") ej2; endif