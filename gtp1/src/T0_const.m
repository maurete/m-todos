function [T] = T0_const ( x, Tc = 0 )
  N = length(x);

  if Tc == 0
    T = zeros(size(x));
  else
    T = ones(size(x));
    T *= Tc;
  endif
 endfunction