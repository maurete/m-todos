#!/usr/bin/octave3.0 -qf
function ej3
  set(0, "defaultlinelinewidth", 2);
  figure

  # defino x
  x=0:1/5:1;

  # defino t1 y t2, para calcular el orden de la soln exacta
  t1=0:1/100:2;
  t2=0:1/200:2;

  # calculo para t1, t2
  T1=ec_calor_cn(x,t1,1,1,0.5,0,"Q_ej1b","","d",1,"n",0);
  T2=ec_calor_cn(x,t2,1,1,0.5,0,"Q_ej1b","","d",1,"n",0);

  # obtengo la T "exacta" mediante extrapolación de Richardson
  c=(200/100)^2;
  for i=1:201
    Te(i,:) = (T1(i,:)-(T2((2*i-1),:)*c))/(1-c);
  endfor

  # calculo T para distintos dt, guardo los valores en los
  # instantes t=0.2 y t=1.5 segundos
  dts=[ 1/5 1/10 1/20 1/50 1/100 1/120 1/150 1/200 1/300 1/500 ];
  e1= zeros(1,length(dts));
  e2= zeros(1,length(dts));

  for i=1:length(dts)
    dt=dts(i);
    t=0:dt:2;
    T=ec_calor_cn(x,t,1,1,0.5,0,"Q_ej1b","","d",1,"n",0);

    # guardo el error en t=0.2s y t=1.5s
    # uso integración por regla del trapecio
    e1(i) = trapz(x,abs(Te(21,:)-T(1+round(0.2/dt),:)));
    e2(i) = trapz(x,abs(Te(151,:)-T(1+round(1.5/dt),:)));
  endfor

  loglog( dts, e1,"-r;$E(T|_{t=0.2})$;",dts, e2,"-b;$E(T|_{t=1.5})$;");
  xlabel("$\\Delta t$");
  ylabel("$E$");

  print -dtex "img/ej3.tex";
  print -depsc2 "img/ej3eps.eps";
  shell_cmd("cd img; epstopdf ej3.eps");
  shell_cmd("cd img; epstopdf ej3eps.eps");

endfunction
#
if strcmp(program_name(), "ej3.m") ej3; endif
