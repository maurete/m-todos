function [Q] = Q_ej1b ( x )
  N = length(x);
  Q = ones(size(x));
  for i=1:N
    if x(i) > 1/2
      Q(i)=0;
    endif
  endfor
endfunction