#!/usr/bin/octave3.0 -qf
function ej1
  set(0, "defaultlinelinewidth", 2);
  figure;

  [E N] = error_ej1(2,100,2);
  plot(N,E);#,"-b;$q(x)$;");
  xlabel("$N$");
  ylabel("$E$");
  ca = gca();
  set(ca,"yscale","log");

  print -dtex "img/ej1.tex";
  print -depsc2 "img/ej1eps.eps";
  shell_cmd("cd img; epstopdf ej1.eps");
  shell_cmd("cd img; epstopdf ej1eps.eps");
endfunction
#
if strcmp(program_name(), "ej1.m") ej1; endif
