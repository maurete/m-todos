# función: solve_fdm_1d
# resuelve la ecuación del calor estacionaria 1D en el dominio [x0, x1]
# con condiciones de contorno Dirichlet T0 y T1 en los extremos, con
# una malla de N elementos. La fuente de calor viene dada por f_Q

function [T x]= solve_fdm_1d( x0=0, x1=1, f_Q="Q_ej1", T0=1, T1=0, N=20)
  x = x0:1/N:x1;
  Q = feval( f_Q, x0, x1, N )';

  # Formo la matriz "interior", para los nodos de 2 a N
  M = N-1;
  K = zeros(M);
  for m=1:M
    K(m,m) = -2*1;
    if m>1
      K(m,m-1) = 1;
    endif
    if m<M
      K(m,m+1) = 1;
    endif
  endfor

  # Agrego en Q2 las condiciones de borde Dirichlet
  # dadas por T0, T1
  Q2 = -Q(2:length(Q)-1).*(x(2)-x(1))^2;
  Q2(1) -= T0;
  Q2(M) -= T1;

  # Resuelvo el sistema tridiagonal (TODO: optimizar)
  T = K \ Q2;
  T = [T0; T; T1];
endfunction