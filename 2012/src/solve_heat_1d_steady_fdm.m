function [T x]= solve_heat_1d_steady_fdm( k=1, c=0, Tenv=0, f_Q="", N=20,
					  x0=0, cond_T0="d", val_T0=1, 
		                          x1=1, cond_T1="n", val_T1=0)
  # Genero el dominio de la var. indep. x
  x = x0:1/N:x1;
  dx2 = (x(2)-x(1))^2;

  # K (fi) = -f
  # Genero f
  if length(f_Q) > 0
    f = -feval( f_Q, x0, x1, N)';
    f *= dx2/k;
  else
    f = zeros( size(x));
  endif
  
  f -= c*dx2*Tenv/k;

  # Formo la matriz ( de N+1xN+1 para poder contemplar
  # condiciones de borde Neumann )
  M = N+1;
  K = zeros(M);
  # lleno la matriz K excepto la primera y última fila
  coef_i = -2-c*dx2/k;
  for m=2:M-1
    K(m,m) = coef_i;
    if m>1
      K(m,m-1) = 1;
    endif
    if m<M
      K(m,m+1) = 1;
    endif
  endfor

  M0=1;
  M1=M;

  # Considero las condiciones en los extremos:
  #   si es Dirichlet, dejo la primer(última) fila en cero
  #   y sumo el valor impuesto de T en la 2da pos de Q;
  #   si es Neumann, agrego los coef. correspondientes
  #   en la primera(últ) fila
  if cond_T0=="d" | cond_T0=="D"
    f(2) -= val_T0;
    M0 = 2;
  elseif cond_T0=="n" | cond_T0=="N"
    K(1,1) = -1;
    K(1,3) = 1;
    f(1) = -2*val_T0*(x(2)-x(1))/k;
    M0 = 1;
  endif

  if cond_T1=="d" | cond_T1=="D"
    f(M-1) -= val_T1;
    M1 = M-1;
  elseif cond_T1=="n" | cond_T1=="N"
    K(M,M) = 1;
    K(M,M-2) = -1;
    f(M) = -2*val_T1*(x(2)-x(1))/k;
    M1 = M;
  endif

  # resuelvo el sistema generado, eliminando filas nulas
  T = K(M0:M1,M0:M1) \ f(M0:M1);
  if M0 > 1
    T=[ val_T0; T];
  endif
  if M1 < M
    T=[ T; val_T1];
  endif

endfunction