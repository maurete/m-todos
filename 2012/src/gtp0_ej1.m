function gtp0_ej1
  figure;
  hold on;
  set(0, "defaultlinelinewidth", 2);

  L=1;
  A=exp(-L)/(exp(L)-exp(-L));
  x=0:L/1000:L-L/1000;

  T=A*exp(x)-(A+1)*exp(-x)+1;
  plot(x/L,T,"-r;$T(x)$;")
  
  q=-A*exp(x)-A*exp(-x)-exp(-x);
  plot(x/L,q,"-b;$q(x)$;")

  xlabel("$x$");
  ylabel("$T(x)$, $q(x)$");
  #title("Soluci\\'on anal\\'itica T(x) y flujo q(x)");
  print -dtex "-S792,560" "img/gtp0_ej1.tex";
  #print -depsc2 "-S800,500" "img/gtp0_ej1eps.eps";
  system("cd img; epstopdf gtp0_ej1.eps");
  hold off;
endfunction
