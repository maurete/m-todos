function [T x] = ej1_exacta( x0=0, x1=1, N=20 )
  x = x0:1/N:x1;
  T = zeros(size(x));
  for i=1:length(x)
    if x(i)>=0 & x(i)<=0.5
      T(i) = -x(i)^2/2-5*x(i)/8+1;
    elseif x(i)>0.5 & x(i)<=1
      T(i)=9*(1-x(i))/8;
    endif
  endfor
endfunction