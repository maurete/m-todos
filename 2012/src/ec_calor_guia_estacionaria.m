function [T Tf]= ec_calor_guia_estacionaria( x, dt, k=1, c=0, Tamb=0, f_Q="", T0="",
			     cond_x0="d", val_x0=1, cond_x1="n", val_x1=0)

  # Calculo dx, dt, Nx, Nt (supongo dx, dt uniformes)
  dx = x(2)-x(1);
  dx2 = dx^2;
  Nx = length(x)-1;
  
#  dt = t(2)-t(1);
  Nt = 20000;

  gamma = k*dt/dx2;

  # Chequeo la condición de Fourier
  if gamma > 0.5
    printf( "NO SE CUMPLE LA CONDICIÓN DE FOURIER!\n" );
  endif

  # Calculo T0
  if length(T0) > 0
    T = feval( T0, x);
  else
    T = T0_const(x, Tamb);
  endif

  # F_i = delta_t * ( Q_i - T_ambiente )
  if length(f_Q) > 0
    Q = feval( f_Q, x);
  else
    Q = zeros( size(x));
  endif

  # En este punto ya tengo T en el tiempo 0.

  # itero en el tiempo: FTCS
  for m=2:Nt
    
    # inicializo un nuevo T para el tiempo m*dt
    T = [ T; zeros( 1, Nx+1) ];

    # considero la condición de borde en x0
    if cond_x0=="d"
      T(m,1) = val_x0;
    elseif cond_x0=="n"
      # calculo el punto ficticio fi_-1 en la grilla en el tiempo anterior
      # éste viene dado de despejar -k*(dfi1 - dfi-1)/dx = val_T0
      fict = T(m-1,2) + dx*val_x0/k;
      # ahora calculo fi_0 como cualquier otro punto interior
      T(m,1) = (1-2*gamma)*T(m-1,1) + gamma*(fict + T(m-1,2)) - dt*(Q(1)+c*(T(m-1,1)-Tamb));
    endif

    # itero en los puntos interiores
    for i=2:Nx
      T(m,i) = (1-2*gamma)*T(m-1,i) + gamma*(T(m-1,i-1)	+ T(m-1,i+1))- dt*(Q(i)+c*(T(m-1,i)-Tamb));
    endfor

    # considero la condición de borde en xL
    if cond_x1=="d" | cond_x1=="D"
      T(m,Nx+1) = val_x1;
    elseif cond_x1=="n" | cond_x1=="N"
      fict = T(m-1,Nx) - dx*val_x1/k;
      T(m,Nx+1) = (1-2*gamma)*T(m-1,Nx+1) + gamma*(T(m-1,Nx) + fict) - dt*(Q(Nx+1)+c*(T(m-1,Nx+1)-Tamb));
    endif

    if sum(abs(T(m-1,:)-T(m,:))) < 1/100
      Tf = T(m,:);
      break;
    endif

  endfor

endfunction