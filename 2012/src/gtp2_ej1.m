#!/usr/bin/octave -qf
# GTP2, Ejercicio 1: visualiza cómo disminuye el error
# al aumentar el número de términos M en la función aproximante.

function gtp2_ej1
  addpath( "../lib" );

  x=0:1/200:1;
  exacta=gtp2_ej1_func(x);
  M = 0:20;
  EP = zeros(size(M));
  EG = zeros(size(M));

  for n=1:length(M)
    EP(n)=trapz(x,wrm_func_poly_pcoll( "gtp2_ej1_func", x, M(n))-exacta);
    EG(n)=trapz(x,wrm_func_poly_galerkin( "gtp2_ej1_func", x, M(n))-exacta);
  endfor

  figure;
  set(0, "defaultlinelinewidth", 2);
  hold on;
  plot(M,abs(EP),"-b;Colocación puntual;");
  plot(M,abs(EG),"-r;Galerkin;");
  xlabel("$M$");
  ylabel("$E$");

  set(gca(),"yscale","log");
  print -dtex "-S800,500" "img/gtp2_ej1.tex";
  print -depsc2 "-S800,500" "img/gtp2_ej1eps.eps";

  shell_cmd("cd img; epstopdf gtp2_ej1.eps");
  shell_cmd("cd img; epstopdf gtp2_ej1eps.eps");
endfunction
#
## if strcmp(program_name(), "gtp2_ej2.m")
##   ej1;
## endif
