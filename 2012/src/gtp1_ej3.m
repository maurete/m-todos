#/usr/bin/octave -qf
# gtp1_ej3: ejercicio 3 de la GTP1 2012
function gtp1_ej3
  addpath( "../lib" );

  x=0:0.5:3;
  y=[1:-0.25:0]';
  [xx yy]=meshgrid(x,y);

  Q=[];
  for i=1:length(x)
    for j=1:length(y)
      Q(j,i) = 2*(y(j)^2+x(i)^2);
    endfor
  endfor

  T=heatfdm_2d_sta(x,y,1,0,0,0,Q,"d",100,"d",100,"d",100,"d",100);
  set(0, "defaultlinelinewidth", 2);
  figure;
  mesh(xx,yy,T);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$T$");
  ca = gca();
  print -dtex "-S800,500" "img/gtp1_ej3.tex";
  print -depsc2 "-S800,500" "img/gtp1_ej3eps.eps";
  system("cd img; epstopdf gtp1_ej3.eps");
  system("cd img; epstopdf gtp1_ej3eps.eps");

  #punto b, contornos T=100
  T=heatfdm_2d_sta(x,y,1,0,0,0,Q,"d",100,"d",100,"d",100,"d",100);
  set(0, "defaultlinelinewidth", 2);
  figure;
  mesh(xx,yy,T);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$T$");
  ca = gca();
  print -dtex "-S800,500" "img/gtp1_ej3_b.tex";
  print -depsc2 "-S800,500" "img/gtp1_ej3_beps.eps";
  system("cd img; epstopdf gtp1_ej3_b.eps");
  system("cd img; epstopdf gtp1_ej3_beps.eps");

  # punto c, contorno S neumann=0
  T=heatfdm_2d_sta(x,y,1,0,0,0,Q,"d",100,"d",100,"d",100,"n",0);
  set(0, "defaultlinelinewidth", 2);
  figure;
  mesh(xx,yy,T);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$T$");
  ca = gca();
  print -dtex "-S800,500" "img/gtp1_ej3_c.tex";
  print -depsc2 "-S800,500" "img/gtp1_ej3_ceps.eps";
  system("cd img; epstopdf gtp1_ej3_c.eps");
  system("cd img; epstopdf gtp1_ej3_ceps.eps");

endfunction
#
#if strcmp(program_name(), "gtp1_ej2.m") gtp1_ej2; endif
