# función: ec_calor_cn
# calcula la solución numérica a la ecuación del calor en 1D, mediante un
# esquema de integración temporal Crank-Nicholson:
#  * si theta == 1   => implícito (BTCS),
#  * si theta == 0.5 => semi-implícito (O(dt^2))
#  * si theta == 0   => explícito (FTCS)
# la cond inicial viene dada por T0, la fuente por f_Q, y las condiciones
# de contorno por cond_x0, val_x0, cond_x1, val_x1

function [T]= ec_calor_cn( x, t, k=1, c=0, theta=0.5, Tamb=0, f_Q="", T0="",
			     cond_x0="d", val_x0=1, cond_x1="n", val_x1=0)

  # Calculo dx, dt, Nx, Nt
  dx = x(2)-x(1);
  dx2 = dx^2;
  Nx = length(x)-1;
  dt = t(2)-t(1);
  Nt = length(t);

  gamma = k*dt/dx2;

  # Chequeo la condición de Fourier
  # if gamma > 0.5
  #   printf( "NO SE CUMPLE LA CONDICIÓN DE FOURIER!\n" );
  # endif

  # Evalúo T0 si me lo pasan, sino constante e igual a Tamb
  if length(T0) > 0
    T = feval( T0, x);
  else
    T = T0_const(x, Tamb);
  endif

  # Evalúo Q si me lo pasan, sino la hago cero
  if length(f_Q) > 0
    Q = feval( f_Q, x);
  else
    Q = zeros( size(x));
  endif

  # coeficientes auxiliares
  A = 1+theta*(2*gamma+c);
  B = -1*theta*gamma;
  C = (theta-1)*(2*gamma+c);
  D = gamma*(1-theta);
  E = c*Tamb;

  # K, f forman mi sistema de ecuaciones KT=f en cada instante
  K = zeros(Nx+1,Nx+1);
  f = zeros(Nx+1,1);

  # lleno las filas interiores de la matriz
  for i=2:Nx
    K(i,i)=A;
    K(i,i-1)=B;
    K(i,i+1)=B;
  endfor
  
  # lleno los valores extremos de la matriz
  if cond_x0=="d" | cond_x0=="D"
    K(1,1) = 1; f(1)=val_x0;
  elseif cond_x0=="n" | cond_x0=="N"
    K(1,1) = A;
    K(1,2) = 2*B;
  endif
  if cond_x1=="d" | cond_x1=="D"
    K(Nx+1,Nx+1) = 1; f(Nx+1)=val_x1;
  elseif cond_x1=="n" | cond_x1=="N"
    K(Nx+1,Nx+1) = A;
    K(Nx+1,Nx) = 2*B;
  endif

  # itero en el tiempo
  for m=2:Nt
    
    # inicializo un nuevo T para el tiempo m*dt
    T = [ T; zeros( 1, Nx+1) ];

    # lleno los valores interiores de f
    for i=2:Nx
      f(i) = C*T(m-1,i)+D*(T(m-1,i-1)+T(m-1,i+1))+E+Q(i);
    endfor

    # considero el caso de conds. de borde Neumann
    if cond_x0=="n" | cond_x0=="N"
      f(1) = C*T(m-1,1)+2*D*T(m-1,2)+E+Q(1)+2*dx*val_x0/k;
    endif
    if cond_x1=="n" | cond_x1=="N"
      f(Nx+1) = C*T(m-1,Nx+1)+2*D*T(m-1,Nx)+E+Q(Nx)-2*dx*val_x1/k;
    endif

    # obteno el vector solución T en el instante actual
    T(m,:) =(K\f)';

  endfor

endfunction