function [Q x] = Q_ej1 ( x0=0, x1=1, N=20 )
  x = x0:1/N:x1;
  Q = ones(size(x));
  for i=1:length(x)
    if x(i) > 1/2
      Q(i)=0;
    endif
  endfor
endfunction