# función: error_ej1
# calcula el error entre la solución exacta del problema de calor
# estacionario del ej. 1, y la solución numérica, para distintos N
# entre N0 y N1, con un paso (delta N) dado por step.

function [E N]= error_ej1( N0=2, N1=100, step=10 )
  N = N0:step:N1;
  E = zeros(size(N));

  for i=1:length(N)
    [fdm ign] = solve_fdm_1d(0, 1, "Q_ej1", 1, 0, N(i));
    [exa ign] = ej1_exacta( 0, 1, N(i));
    E(i) = sum((fdm' - exa).^2);
  endfor

endfunction
