% GTP2 ejercicio 3: obtiene una solución aproximada al
% problema de difusión térmica en 2D.

function gtp2_ej3_matlab
  x=-1:1/50:1;
  y=-1:1/50:1;

  psi = @(x,y) (2-x.^2)*(2-y.^2)-1;

  M = 1:5;
  E = zeros(size(M));
  a = zeros(1,max(M));

  % inicializo fi, el valor de la función en el rectángulo
  fi = zeros(length(x),length(y));
  for i=1:length(x)
    for j=1:length(y)
      fi(i,j) = psi(x(i),y(j));
    end
  end

  % calculo cada coeficiente a_m y agrego a fi a_m *N_m
  for m = 1:M(1)
    C = ((2*m-1)*pi/2);
    Kmm = @(x,y) (cos( x.*C ).^2).*(cos( y.*C ).^2).*(-2*C^2);
    fm = @(x,y) (cos(x.*C).*(cos(y.*C)).*((x.^2)+(y.^2)-4).*(2/pi)^2);
    a(m) = dblquad(fm,-1,1,-1,1) / dblquad(Kmm,-1,1,-1,1);
    for i=1:length(x)
      for j=1:length(y)
        fi(i,j) = fi(i,j) + a(m)*cos(C*x(i))*cos(C*y(j));
      end
    end
  end

  % calculo cada coeficiente a_m y agrego a fi a_m *N_m
  for n=2:length(M)
    for m = (M(n-1)+1):M(n)
      C = ((2*m-1)*pi/2);
      Kmm = @(x,y) (cos( x.*C ).^2).*(cos( y.*C ).^2).*(-2*C^2);
      fm = @(x,y) (cos(x.*C).*(cos(y.*C)).*((x.^2)+(y.^2)-4).*(2/pi)^2);
      a(m) = dblquad(fm,-1,1,-1,1) / dblquad(Kmm,-1,1,-1,1);
      for i=1:length(x)
        for j=1:length(y)
          fi(i,j) = fi(i,j) + a(m)*cos(C*x(i))*cos(C*y(j));
        end
      end
    end
  end

  % grafico el resultado
  figure
  h = mesh(x,y,fi);
  view([45,25]);
  set(gca,'CameraViewAngleMode','Manual');
  camzoom(3)
  %axis([-1 1 -1 1 0 5])
  set(gca,'PlotBoxAspectRatio',[5 5 4]);
  xlabel('x')
  ylabel('y')
  zlabel('\phi')
  print -depsc 'img/gtp2_ej3eps.eps'
  system('cd img; epstopdf gtp2_ej3eps.eps');

end
