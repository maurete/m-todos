#!/usr/bin/octave3.2 -qf
function gtp2_ej3

  x=-1:1/50:1;
  y=-1:1/50:1;

  psi = @(x,y) (2-x.^2)*(2-y.^2)-1;

  M = 1:5;
  E = zeros(size(M));
  a = zeros(1,max(M));

  % inicializo fi, el valor de la función en el rectángulo
  fi = zeros(length(x),length(y));
  for i=1:length(x)
    for j=1:length(y)
      fi(i,j) = psi(x(i),y(j));
    end
  end

  # calculo cada coeficiente a_m y agrego a fi a_m *N_m
  for m = 1:M(1)
    C = ((2*m-1)*pi/2);
    Kmm = @(x,y) (cos( x.*C ).^2).*(cos( y.*C ).^2).*(-2*C^2);
    fm = @(x,y) (cos(x.*C).*(cos(y.*C)).*((x.^2)+(y.^2)-4).*(2/pi)^2);
    a(m) = dblquad(fm,-1,1,-1,1) / dblquad(Kmm,-1,1,-1,1);
    for i=1:length(x)
      for j=1:length(y)
        fi(i,j) = fi(i,j) + a(m)*cos(C*x(i))*cos(C*y(j));
      end
    end
  end

  # calculo cada coeficiente a_m y agrego a fi a_m *N_m
  for n=2:length(M)
    for m = (M(n-1)+1):M(n)
      C = ((2*m-1)*pi/2);
      Kmm = @(x,y) (cos( x.*C ).^2).*(cos( y.*C ).^2).*(-2*C^2);
      fm = @(x,y) (cos(x.*C).*(cos(y.*C)).*((x.^2)+(y.^2)-4).*(2/pi)^2);
      a(m) = dblquad(fm,-1,1,-1,1) / dblquad(Kmm,-1,1,-1,1);
      for i=1:length(x)
        for j=1:length(y)
          fi(i,j) = fi(i,j) + a(m)*cos(C*x(i))*cos(C*y(j));
        end
      end
    end
  end

  set(0, "defaultlinelinewidth", 2);
  figure
  mesh(x,y,fi);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$\\phi(x,y)$");
  #title("$\\sigma_x$");
  print -dtex "-S792,560" "img/gtp2_ej3.tex";
  system("cd img; epstopdf gtp2_ej3.eps");

endfunction
#
#if strcmp(program_name(), "ej3.m")
#  ej3;
#endif
