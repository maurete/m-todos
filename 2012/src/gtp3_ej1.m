
function K = gtp3_ej1

    xnod = [ 0 1/4 1/2 3/4 1 ];
    nnod = 1:5;
    elem = 1:4;
    cone = [ 1 2;2 3;3 4;4 5 ];

    nn = length(xnod);
    ne = length(elem);
    
    syms x;
    
    for i=1:ne
        xi = xnod(nnod(cone(i,1)));
        xj = xnod(nnod(cone(i,2)));
        he = xj-xi;
        chi = x-xi;
        Ni = chi/he;
        Nj = (he-chi)/he;
    
    end

    
endfunction
  
  
%   % Condiciones de borde
%   T_left = 1;
%   T_right = 0;
  
%   Kele = zeros(Nelem,2,2);
%   fele = zeros(Nelem,2);
  
%   % Fuente (Revisar esto!)
%   Q = [ones(length(xnode)/2,1);zeros(length(xnode)/2,1)]';

%   %ensamble de cada elemento: matriz y termino derecho
%   for iele=1:Nelem
%     xi = xnode(iele);
%     xj = xnode(iele+1);
%     h = xj-xi;
%     N(1) = (h-(x-xi))/h;
%     N(2) = (x-xi)/h;
%     dNdx = [-1/h 1/h];
%     for il=1:2
%       for jl=1:2
%         integ = dNdx(il)*dNdx(jl);
%         Kele(iele,il,jl) = double(int(integ,x,xi,xj));
%       end
%       integ2 = N(il)*Q(iele); % ver!
%       fele(iele,il) = double(int(integ2,x,xi,xj));
%     end
%   end
  
%   if ~sp
%     Kg = zeros(Nnod,Nnod);
%   else
%     rows = [];
%     cols = [];
%     coef = [];
%   end
%   fg = zeros(Nnod,1);

%   %ensamblo la matriz global y el residuo global
%   for iele=1:Nelem
%     indx_global = [iele iele+1];
%     if sp
%       %i_local(il)=1 -> i_global(ig) = iele;
%       %j_local(jl)=2 -> j_global(jg) = iele+1;
%       for il=1:2
%         ig = indx_global(il);
%         for jl=1:2
%           jg = indx_global(jl);
%           rows = [rows;ig];
%           cols = [cols;jg];    
%           coef = [coef;Kele(iele,il,jl)];
%         end
%         fg(ig) = fg(ig) + fele(iele,il);
%       end
%     else
%       %i_local(il)=1 -> i_global(ig) = iele;
%       %j_local(jl)=2 -> j_global(jg) = iele+1;
%       for il=1:2
%         ig = indx_global(il);
%         for jl=1:2
%           jg = indx_global(jl);
%           Kg(ig,jg) = Kg(ig,jg) + Kele(iele,il,jl);
%         end
%         fg(ig) = fg(ig) + fele(iele,il);
%       end
%     end
%   end
%   if sp
%     Kg = sparse(rows,cols,coef);
%   end

%   % CB Dirichlet
%   % Condicion T(x=0) = 1
%   Kg(1,:) = 0;
%   Kg(1,1) = 1;
%   fg(1) = T_left;
%   % Condicion T(x=1) = 0
%   Kg(Nnod,:) = 0;
%   Kg(Nnod,Nnod) = 1;
%   fg(Nnod) = T_right;

%   % Resolucion del sist. de ecuaciones
%   T = Kg\fg;
  
%   % Solucion analitica
%   T_anal = -5/6*x+1-(x^3)/6;
%   T_ex = subs(T_anal,x,xnode);
  
%   % Grafica de comparacion: analitica vs FEM
%   figure(1);clf;plot(xnode,T,'o-',xnode,T_ex,'r');

end
