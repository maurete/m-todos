function gtp0_ej3_2
  Lx=1;
  Ly=1;
  x=0:Lx/100:Lx-Lx/100;
  y=0:Ly/100:Ly-Ly/100;
  ex=zeros(100,100);
  ey=zeros(100,100);
  exy=zeros(100,100);
  for i=1:100, j=1:100;
    ex(i,j) =(cos(pi*y(j)/(2*Ly))*(-0.1))'*(cos(pi*x(i)/Lx)*(pi/Lx)); #'
    ey(i,j) =(sin(pi*x(i)/(2*Lx))*0.1).*(sin(pi*y(j)/Ly)*(pi/Ly));
    exy(i,j)=(sin(pi*x(i)/Lx)*0.1).*(sin(pi*y(j)/(2*Ly))*(pi/(2*Ly))) \
            -(cos(pi*y(j)/Ly)*0.1).*(cos(pi*x(i)/(2*Lx))*(pi/(2*Lx)));
  endfor;

  #
  # 2do item: stresses
  #
  E=200;
  nu=0.28;
  sx=zeros(100,100);
  sy=zeros(100,100);
  sxy=zeros(100,100);
  ca1=E/(1-nu^2); #const. auxiliar 1
  ca2=ca1*(1-nu); #const aux. 2 (para sxy)
  for i=1:100, j=1:100;
    sx(i,j) = ca1*(ex(i,j)+nu*ey(i,j));
    sy(i,j) = ca1*(nu*ex(i,j)+ey(i,j));
    sxy(i,j)= ca2*exy(i,j);
  endfor;
  figure;
  mesh(x,y,sx);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$\\sigma_x(x,y)$");
  title("$\\sigma_x$");
  print -dtex "-S792,560" "img/gtp0_ej3_2a.tex";
  system("cd img; epstopdf gtp0_ej3_2a.eps");
  figure;
  mesh(x,y,sy);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$\\sigma_y(x,y)$");
  title("$\\sigma_y$");
  print -dtex "-S792,560" "img/gtp0_ej3_2b.tex";
  system("cd img; epstopdf gtp0_ej3_2b.eps");
  figure;
  mesh(x,y,sxy);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$\\sigma_{xy}(x,y)$");
  title("$\\sigma_{xy}$");
  print -dtex "-S792,560" "img/gtp0_ej3_2c.tex";
  system("cd img; epstopdf gtp0_ej3_2c.eps");
endfunction
