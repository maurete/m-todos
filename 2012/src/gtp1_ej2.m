#/usr/bin/octave -qf
# gtp1_ej2: ejercicio 2 de la GTP1 2012
function gtp1_ej2
  addpath( "../lib" );
  sample= [ 1:99 100:100:5000 ];
  error=zeros(size(sample));
  for i=1:length(sample)
    x=0:1/sample(i):1;
    # soln exacta
    k1=(6-4*e)/(1-e^2);
    k2=k1-6;
    Tex=k1*exp(x)+k2*exp(-x)-4*x.^2+4*x-8;
    # soln numérica
    Q=1-4*(x-0.5).^2;
    Tnum=heatfdm_1d_sta(x,1,1,0,Q,"n",10,"n",0);
    # calculo el error entre las soluciones
    error(i) = sum( (Tnum-Tex').^2 );
  endfor
  set(0, "defaultlinelinewidth", 2);
  figure;
  loglog(sample, error);
  xlabel("$N$");
  ylabel("$E$");
  ca = gca();
  print -dtex "-S800,500" "img/gtp1_ej2.tex";
  print -depsc2 "-S800,500" "img/gtp1_ej2eps.eps";
  system("cd img; epstopdf gtp1_ej2.eps");
  system("cd img; epstopdf gtp1_ej2eps.eps");
endfunction
#
#if strcmp(program_name(), "gtp1_ej2.m") gtp1_ej2; endif
