function gtp0_ej3_1
  Lx=1;
  Ly=1;
  x=0:Lx/100:Lx-Lx/100;
  y=0:Ly/100:Ly-Ly/100;
  ex=zeros(100,100);
  ey=zeros(100,100);
  exy=zeros(100,100);
  for i=1:100, j=1:100;
    ex(i,j) =(cos(pi*y(j)/(2*Ly))*(-0.1))'*(cos(pi*x(i)/Lx)*(pi/Lx)); #'
    ey(i,j) =(sin(pi*x(i)/(2*Lx))*0.1).*(sin(pi*y(j)/Ly)*(pi/Ly));
    exy(i,j)=(sin(pi*x(i)/Lx)*0.1).*(sin(pi*y(j)/(2*Ly))*(pi/(2*Ly))) \
            -(cos(pi*y(j)/Ly)*0.1).*(cos(pi*x(i)/(2*Lx))*(pi/(2*Lx)));
  endfor;
  figure;
  mesh(x,y,ex);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$\\epsilon_x(x,y)$");
  title("$\\epsilon_x$");
  print -dtex "-S792,560" "img/gtp0_ej3_1a.tex";
  system("cd img; epstopdf gtp0_ej3_1a.eps");
  figure;
  mesh(x,y,ey);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$\\epsilon_y(x,y)$");
  title("$\\epsilon_y$");
  print -dtex "-S792,560" "img/gtp0_ej3_1b.tex";
  system("cd img; epstopdf gtp0_ej3_1b.eps");
  figure;
  mesh(x,y,exy);
  xlabel("$x$");
  ylabel("$y$");
  zlabel("$\\epsilon_{xy}(x,y)$");
  title("$\\epsilon_{xy}$");
  print -dtex "-S792,560" "img/gtp0_ej3_1c.tex";
  system("cd img; epstopdf gtp0_ej3_1c.eps");
endfunction
