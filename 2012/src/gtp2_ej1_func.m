#!/usr/bin/octave -qf
# GTP2, Ejercicio 1: función a aproximar
function y=gtp2_ej1_func(x)
  y=sin(x.*pi/2)+1;
endfunction
