function y=fi2(x)
y=log(15*x+1)+0.2*sin(20*x*pi/2)+1;
endfunction
