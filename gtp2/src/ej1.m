#!/usr/bin/octave -qf
# GTP2, Ejercicio 1: visualiza cómo disminuye el error
# al aumentar el número de términos M en la función aproximante.

function ej1
  figure;
  set(0, "defaultlinelinewidth", 2);

  x=0:1/200:1;
  exacta=fi(x);
  M = 0:40;
  EP = zeros(size(M));
  EG = zeros(size(M));

  for n=1:length(M)
    EP(n)=trapz(x,mrp_func_poly_pcoll( "fi", x, M(n))-exacta);
    EG(n)=trapz(x,mrp_func_poly_galerkin( "fi", x, M(n))-exacta);
  endfor

  hold on;
  plot(M,abs(EP),"-b;Colocaci\\'on puntual;");
  plot(M,abs(EG),"-r;Galerkin;");
  xlabel("$M$");
  ylabel("$E$");

  set(gca(),"yscale","log");
  print -dtex "img/ej1.tex";
  print -depsc2 "img/ej1eps.eps";

  shell_cmd("cd img; epstopdf ej1.eps");
  shell_cmd("cd img; epstopdf ej1eps.eps");
endfunction
#
if strcmp(program_name(), "ej1.m")
  ej1;
endif