# GTP2 ej1: función a aproximar
function y=fi(x)
  y=sin(x.*pi/2)+1;
endfunction
