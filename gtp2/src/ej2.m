#!/usr/bin/octave3.0 -qf
# GTP2, ej2: resuelve el problema de transferencia de calor mediante
# Galerkin, y genera una gráfica No. de términos M - error de
# aproximación E.
function ej2
  set(0, "defaultlinelinewidth", 2);

  # para la gráfica M-E
  M = 1:10;
  E = zeros(size(M));

  # genero el dominio x, la soln. exacta y una fi inicial =0
  x=0:1/200:1;
  exacta=sin(x)*(sin(1)-cos(1)+1)/(cos(1)+sin(1))+cos(x)-1;
  fi = zeros(size(x));

  # calculo K, f...
  for n = 1:length(M)
    K=zeros(M(n),M(n));
    f=zeros(M(n),1);
    for l=1:M(n)
      for m=l:M(n)
	K(l,m) = l*m/(l+m-1) + 1/(l+m+1)+1;
	K(m,l) = K(l,m);
	f(l) = -1/(l+1);
      endfor
    endfor

    # calculo a
    a = K\-f;

    # armo la funcion aproximante
    fi = zeros(size(x));
    for m=1:length(a)
      fi += (x.^m).*a(m);
    endfor

    # calculo el error cometido con M(n) términos
    E(n)=trapz(x,fi-exacta);
  endfor

  # grafico la exacta y aproximada
  figure
  hold on;
  plot(x,exacta,"-r;$\\phi (x)$;");
  plot(x,fi,"-b;$\\hfi (x)$;");
  xlabel("$x$");
  print -dtex "img/ej2b.tex";
  shell_cmd("cd img; epstopdf ej2b.eps");
  
  # grafico M vs error
  figure
  plot(M,abs(E));
  xlabel("$M$"); ylabel("$E$");
  # set(gca(),"yscale","log");
  print -dtex "img/ej2.tex";
  shell_cmd("cd img; epstopdf ej2.eps");
endfunction
#
if strcmp(program_name(), "ej2.m")  ej2; endif