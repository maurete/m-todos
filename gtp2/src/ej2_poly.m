#!/usr/bin/octave -qf
function ej2_poly
  set(0, "defaultlinelinewidth", 2);

  x=1/200:1/200:1;
  exacta=sin(x)*(sin(1)-cos(1)+1)/(cos(1)+sin(1))+cos(x)-1;

  M = 1:20;
  E = zeros(size(M));
  a = zeros(1,max(M));

  # empiezo con una fi cero constante

  # K=zeros(M(1));
  # f=zeros(M(1),1);
  # for l=1:M(1)
  #   for m = 1:M(1)
  #     integrando = @(x) (x.^l).*( (x.^(m-2)).*(m*(m-1)) );
  #     K(l,m) = trapz(x,integrando(x));
  #     f(l) = -trapz(x,x.^l);
  #   endfor
  # endfor
  # a(1:M(1)) = K\f;

  # fi = zeros(size(x));
  # for m=1:M(1)
  #   fi += (1-x).*(x.^m).*a(m);
  # endfor
  # E(1)=trapz(x,fi-exacta);

fi=0;

  for n=1:length(M)
    K=zeros(M(n));
    f=zeros(M(n),1);
    for l = 1:M(n)
      for m=1:M(n)
	integrando = @(x) (x.^l).*( (x.^(m-2)).*(m^2-m) );
	K(l,m) = trapz(x,(x.^l).*( (x.^(m-2)).*(m^2-m) ));
	f(l) = -trapz(x,x.^l);
      endfor
    endfor

    a(1:M(n)) = K\f;

    fi = zeros(size(x));
    for m=1:M(n)
      fi += (1-x).*(x.^m).*a(m);
    endfor
    E(n)=trapz(x,fi-exacta);
  endfor

  figure
  hold on;
  plot(x,exacta,"-r;$\\phi (x)$;");
  plot(x,fi,"-b;$\\hfi (x)$;");
  xlabel("$x$");
  print -dtex "img/ej2b.tex";
  shell_cmd("cd img; epstopdf ej2b.eps");

  figure
  hold on; 
  plot(M,abs(E));
  xlabel("$M$");
  ylabel("$E$");
  ca = gca();
# set(ca,"yscale","log");
#  print -dtex "img/ej2.tex";

#  shell_cmd("cd img; epstopdf ej2.eps");
endfunction
#
#if strcmp(program_name(), "ej2_poly.m")
#  ej2_poly;
#endif
