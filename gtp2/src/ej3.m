#!/usr/bin/octave3.2 -qf
function ej3
  set(0, "defaultlinelinewidth", 2);

  x=0:1/50:1;
  y=0:1/50:1;

  psi = @(x,y) (2-x.^2)*(2-y.^2)-1;

  M = 0:10;
  E = zeros(size(M));
  a = zeros(1,max(M));

  fi = zeros(length(x),length(y));
  for i=1:length(x)
    for j=1:length(y)
      fi(i,j) = psi(x(i),y(j));
    endfor
  endfor

  for m = 1:M(1)
    Kmm = @(x,y) (sin(m*pi*x/2).^2)*(sin(m*pi*y/2).^2);
    fm = @(x,y) (2/pi)^2*sin(m*pi*x/2)*sin(m*pi*y/2)*(x^2+y^2-4);
    a(m) = quad2dg(fm,-1,1,-1,1) / quad2dg(Kmm,-1,1,-1,1);
    for i=1:length(x)
      for j=1:length(y)
	fi(i,j) += a(m)*sin(m*pi*x(i)/2)*sin(m*pi*y(j)/2);
      endfor
    endfor
  endfor

  #E(1)=trapz(x,FI-exacta);

  for n=2:length(M)
    for m = (M(n-1)+1):M(n)
      Kmm = @(x,y) (sin(m*pi*x/2).^2)*(sin(m*pi*y/2).^2);
      fm = @(x,y) (2/pi)^2*sin(m*pi*x/2)*sin(m*pi*y/2)*(x^2+y^2-4);
      a(m) = quad2dg(fm,-1,1,-1,1) / quad2dg(Kmm,-1,1,-1,1);
      for i=1:length(x)
	for j=1:length(y)
	  fi(i,j) += a(m)*sin(m*pi*x(i)/2)*sin(m*pi*y(j)/2);
	endfor
      endfor
    endfor
    
    #E(n)=trapz(x,FI-exacta);
  endfor

  a
  figure
  hold on;
  mesh(x,y,fi);
 # plot(x,exacta,"-r;$\\phi (x)$;");
#  plot(x,FI,"-b;$\\hfi (x)$;");
#  xlabel("$x$");
#  print -dtex "img/ej2b.tex";
#  shell_cmd("cd img; epstopdf ej2b.eps");
 endfunction
#
if strcmp(program_name(), "ej3.m")
  ej3;
endif